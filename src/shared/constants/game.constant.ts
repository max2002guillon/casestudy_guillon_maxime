export class GameConstants {
  public static readonly FRAME_URL = 'assets/svg/';

  public static readonly ITEM_ID_PREFIX = 'item-';

  public static readonly CLICKABLE_CLASS = 'clickable';
  public static readonly DISPLAY_NONE_CLASS = 'd--none';

  public static readonly DIALOG_INTRO = 'dialog-intro';
  public static readonly DIALOG_INFO_SUCCESS = 'dialog-info-success';

  public static readonly DIALOG_OPENING = 'dialog__opening';
  public static readonly DIALOG_TALKING = 'dialog__talking';

  public static readonly INTRO_CLOSE_TIMEOUT = 5000;

  public static readonly MAX_PLAYABLE_TIME = 50 * 60 * 1000;

  public static readonly PRELOAD_MOBILE_PREFIX = 'MOBILE';

  public static readonly CLICK_ANIMATION_VISIBLE_DELAY = 2 * 60 * 1000;
}


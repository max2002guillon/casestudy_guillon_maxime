export class AuthConstants {
  public static readonly USER_CREDENTIAL = 'USER_CREDENTIAL';
  public static readonly LOCALE_STORAGE_KEY = 'emeraude-v2-locale';
}

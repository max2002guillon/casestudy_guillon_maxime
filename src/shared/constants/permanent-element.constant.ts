export class PermanentElementID {
  public static readonly TIMER = 'game-timer';
  public static readonly NAVIGATION_ARROW = 'game-navigation-arrow';
  public static readonly CHATBOT = 'game-chatbot';
  public static readonly INVENTORY = 'game-inventory';
  public static readonly INVENTORY_OPEN = 'game-inventory-open';
  public static readonly PROGRESS = 'game-progress';
  public static readonly BACK_BTN = 'game-back-btn';
  public static readonly SOUND = 'game-sound';
  public static readonly WEBCAMS = 'game-webcams';
  public static readonly KEY = 'game-key';
}

import { GameDialogData } from '../components/game-dialog/interfaces/game-dialog-data.interface';

export interface GameFrameItem {
  id: string;
  classes?: string[];
  dialogData?: GameDialogData;
  mousedown?: (event: MouseEvent) => void;
  mouseup?: (event: MouseEvent) => void;
  mouseenter?: (event: MouseEvent) => void;
  mouseleave?: (event: MouseEvent) => void;
  focusout?: (event) => void;
}

import { CdkDragEnd, CdkDragStart } from '@angular/cdk/drag-drop';
import { GameDialogData } from '../components/game-dialog/interfaces/game-dialog-data.interface';

export interface GameInventoryItem {
  id: string;
  imgSrc?: string;
  imgSrcActive?: string;
  isActive?: boolean;
  dialogData?: GameDialogData;
  hidden?: boolean;
  canDrag?: boolean;
  hiddenFromBackground?: boolean;
  onClick?: () => void;
  onDragStarted?: ($event: CdkDragStart) => void;
  onDragEnded?: ($event: CdkDragEnd) => void;
}

export interface GameTimerConfig {
  initialDelay?: number;
  interval?: number;
}

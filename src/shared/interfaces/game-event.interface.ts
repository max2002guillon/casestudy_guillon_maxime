export interface GameEvent {
  name: string | number;
  value?: any;
  config?: any;
  isFrameMultiLanguage?: boolean;
}

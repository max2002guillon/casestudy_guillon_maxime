export interface ElementPosition {
  left: number;
  top: number;
  right: number;
  bottom: number;
}
export interface PermanentElementPosition extends ElementPosition {
  elementId: string;
}

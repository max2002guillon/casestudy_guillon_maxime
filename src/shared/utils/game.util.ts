import { GameConstants } from '../constants/game.constant';

export class GameUtil {
  public static hideElementsById(ids: string[]) {
    ids.forEach(id => this.hideElementById(id));
  }

  public static hideElementById(id: string) {
    const element = document.getElementById(id);
    this.hideElement(element);
  }

  public static hideElement(element: Element) {
    if (!element) { return; }
    element.classList.add(GameConstants.DISPLAY_NONE_CLASS);
  }

  public static showElementsById(ids: string[]) {
    ids.forEach(id => this.showElementById(id));
  }

  public static showElementById(id: string) {
    const element = document.getElementById(id);
    this.showElement(element);
  }

  public static showElement(element: Element) {
    if (!element) { return; }
    element.classList.remove(GameConstants.DISPLAY_NONE_CLASS);
  }

  public static focusCodeInput(selector?: string, isFocusLastChar = false) {
    const defaultSelector = !isFocusLastChar ? 'code-input input' : 'code-input span:last-child input';
    selector = selector || defaultSelector;
    const input = document.querySelector(selector) as HTMLInputElement;
    if (!input) {
      return;
    }
    input.focus();
    input.select();
    input.scrollIntoView({ block: 'nearest', inline: 'nearest' });
  }

  public static focusElement(selector: string) {
    const defaultSelector = '.mat-typography';
    selector = selector || defaultSelector;
    const element = document.querySelector(selector) as HTMLElement;
    if (!element) {
      return;
    }
    element.focus();
  }

  public static getBrowserSize(): { width: number, height: number } {
    const width = window.innerWidth || document.documentElement.clientWidth ||
      document.body.clientWidth;
    const height = window.innerHeight || document.documentElement.clientHeight ||
      document.body.clientHeight;
    return { width, height };
  }
}

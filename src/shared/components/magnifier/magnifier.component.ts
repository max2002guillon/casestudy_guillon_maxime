import { Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-magnifier',
  templateUrl: './magnifier.component.html',
  styleUrls: ['./magnifier.component.scss'],
})
export class MagnifierComponent extends BaseComponent implements OnChanges {
  @Input() isEnabled: boolean;
  @Input() originImageUrl: string;
  @Input() zoomImageUrl: string;
  @Input() lensBorderWidth = 6;
  @Input() lensBorderColor: string;
  @Input() additionalElementSelector: string;

  @ViewChild('section') sectionElementRef: ElementRef;
  @ViewChild('container') containerElementRef: ElementRef;
  @ViewChild('glass') glassElementRef: ElementRef;
  @ViewChild('lens') lensElementRef: ElementRef;
  @ViewChild('originImage') originImageElementRef: ElementRef;

  public width: number;
  public height: number;
  public lensBackgroundImage: string;
  public lensBackgroundSize: string;

  private windowResizeEvent: (ev: UIEvent) => void;

  private get sectionElement(): HTMLDivElement {
    return this.sectionElementRef.nativeElement;
  }

  private get containerElement(): HTMLDivElement {
    return this.containerElementRef.nativeElement;
  }

  private get glassElement(): HTMLDivElement {
    return this.glassElementRef.nativeElement;
  }

  private get lensElement(): HTMLDivElement {
    return this.lensElementRef.nativeElement;
  }

  private get originImageElement(): HTMLImageElement {
    return this.originImageElementRef.nativeElement;
  }

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.zoomImageUrl) {
      this.setLensBackgroundImage();
    }
  }

  onAfterViewInit() {
    this.detectSize();
    this.initEvents();
  }

  onDestroy() {
    window.removeEventListener('resize', this.windowResizeEvent);
  }

  private detectSize() {
    const resizeInterval = setInterval(() => {
      if (!this.originImageUrl) {
        clearInterval(resizeInterval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 500);

    this.windowResizeEvent = (ev: UIEvent) => {
      const width = this.sectionElement.clientWidth;
      const height = this.sectionElement.clientHeight;
      if (width === 0) {
        return;
      }
      clearInterval(resizeInterval);

      this.width = width;
      this.height = height;
      this.setLensBackgroundSize();
    };

    this.windowResizeEvent.bind(this);
    window.addEventListener('resize', this.windowResizeEvent);
  }

  private setLensBackgroundImage() {
    const value = `url(${this.zoomImageUrl})`;
    this.lensBackgroundImage = value;
  }

  private setLensBackgroundSize() {
    const value = this.width + 'px ' + this.height + 'px';
    this.lensBackgroundSize = value;
  }

  private initEvents() {
    this.containerElement.onmouseenter = ev => {
      this.showGlass();
    };
    this.containerElement.onmouseleave = ev => {
      this.hideGlass();
    };
    this.containerElement.onmousemove = ev => {
      this.moveGlass(ev);
    };
    if (this.additionalElementSelector) {
      const elements = Array.from(document.querySelectorAll(this.additionalElementSelector)) as HTMLElement[];
      elements.forEach(el => {
        el.onmouseenter = ev => this.showGlass();
        el.onmousemove = ev => this.moveGlass(ev);
      });
    }
  }

  private showGlass() {
    if (!this.isEnabled) {
      return;
    }
    this.glassElement.classList.remove('hidden');
  }

  private hideGlass() {
    this.glassElement.classList.add('hidden');
  }

  /**
   * @see https://www.w3schools.com/howto/howto_js_image_magnifier_glass.asp
   */
  private moveGlass(ev: MouseEvent | TouchEvent) {
    if (!this.isEnabled) {
      return;
    }
    const glass = this.glassElement;
    const lens = this.lensElement;
    const image = this.originImageElement;
    const zoom = 1;
    const glassBorderWidth = this.lensBorderWidth;
    const glassOffset = 0; // Because of magnifier.svg
    const glassWidth = glass.offsetWidth / 2 - glassOffset;
    const glassHeight = glass.offsetHeight / 2 - glassOffset;
    // Prevent any other actions that may occur when moving over the image
    ev.preventDefault();
    // Get the cursor's x and y positions
    let { x, y } = this.getCursorPosition(ev);
    // Prevent the magnifier glass from being positioned outside the image
    if (x > image.width - (glassWidth / zoom)) {
      x = image.width - (glassWidth / zoom);
    }
    if (x < glassWidth / zoom) {
      x = glassWidth / zoom;
    }
    if (y > image.height - (glassHeight / zoom)) {
      y = image.height - (glassHeight / zoom);
    }
    if (y < glassHeight / zoom) {
      y = glassHeight / zoom;
    }
    // Set the position of the magnifier glass
    glass.style.left = (x - glassWidth) + 'px';
    glass.style.top = (y - glassHeight) + 'px';
    // Set the background position of the magnifier lens
    lens.style.backgroundPosition = [
      (x * zoom) - glassWidth + glassBorderWidth,
      (y * zoom) - glassHeight + glassBorderWidth,
    ].map(v => `-${v}px`).join(' ');
  }

  private getCursorPosition(ev) {
    const image = this.originImageElement;
    // Get the x and y positions of the image
    const rect = image.getBoundingClientRect();
    let x = 0;
    let y = 0;
    // Calculate the cursor's x and y coordinates, relative to the image
    x = ev.pageX - rect.left;
    y = ev.pageY - rect.top;
    // Consider any page scrolling
    x -= window.pageXOffset;
    y -= window.pageYOffset;
    return { x, y };
  }
}

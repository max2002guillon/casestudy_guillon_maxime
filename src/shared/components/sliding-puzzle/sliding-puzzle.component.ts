import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { wrapGrid } from 'animate-css-grid';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-sliding-puzzle',
  templateUrl: './sliding-puzzle.component.html',
  styleUrls: ['./sliding-puzzle.component.scss'],
})
export class SlidingPuzzleComponent extends BaseComponent implements OnChanges {
  @Input() images: string[];
  @Input() tilePaddingBottom = '100%';

  @Output() completed = new EventEmitter();

  public bgImages: string[];

  private allTiles: HTMLElement[];
  private gridAnimation: () => void;

  private readonly REQUIRED_IMAGE_COUNT = 8;
  private readonly AREA_KEYS = {
    A: ['B', 'D'],
    B: ['A', 'C', 'E'],
    C: ['B', 'F'],
    D: ['A', 'E', 'G'],
    E: ['B', 'D', 'F', 'H'],
    F: ['C', 'E', 'I'],
    G: ['D', 'H'],
    H: ['E', 'G', 'I'],
    I: ['F', 'H'],
  };
  private readonly TIME_CLOSE_DIALOG = 120000;

  ngOnChanges(changes: SimpleChanges): void {
    if (this.images) {
      this.initImages();
    }
  }

  onAfterViewInit() {
    this.initPuzzle();
    setTimeout(() => {
      this.completed.next();
    }, this.TIME_CLOSE_DIALOG);
  }

  private initImages() {
    if (this.images.length !== this.REQUIRED_IMAGE_COUNT) {
      return;
    }
    this.bgImages = this.images.map(v => `url(${v})`);
  }

  /**
   * @see https://simpleweb.co.uk/coding-a-festive-puzzle-game-with-modern-front-end-techniques/
   */
  private initPuzzle() {
    // Initiate CSS Grid animation tool
    const grid = document.querySelector('.grid') as HTMLDivElement;
    const { forceGridAnimation } = wrapGrid(grid);
    this.gridAnimation = forceGridAnimation;

    // Get all the tiles and the empty tile
    const allTiles = Array.from(document.querySelectorAll('.tile')) as HTMLElement[];
    const tiles = allTiles.filter(v => v.nodeName === 'BUTTON') as HTMLButtonElement[];
    const emptyTile = document.querySelector('.tile--empty') as HTMLDivElement;

    this.allTiles = allTiles;
    this.toggleTilesDisabled(tiles, emptyTile);

    // Attach event
    tiles.forEach(tile => {
      tile.onclick = () => {
        this.onTileClick(tile, emptyTile);
        this.toggleTilesDisabled(tiles, emptyTile);
        this.checkComplete();
      };
    });

    // Shuffle
    this.shuffle();
  }

  private getTileArea(el: HTMLElement) {
    const value = el.style.getPropertyValue('--area').trim();
    return value;
  }

  private onTileClick(currentTile: HTMLButtonElement, emptyTile: HTMLDivElement) {
    // Grab the grid area set on the clicked tile and empty tile
    const currentTileArea = this.getTileArea(currentTile);
    const emptyTileArea = this.getTileArea(emptyTile);

    const isValidSwap = this.AREA_KEYS[currentTileArea]?.includes(emptyTileArea);
    if (!isValidSwap) {
      return;
    }

    // Swap the empty tile with the clicked tile
    emptyTile.style.setProperty('--area', currentTileArea);
    currentTile.style.setProperty('--area', emptyTileArea);

    // Animate the tiles
    if (this.gridAnimation) {
      this.gridAnimation();
    }
  }

  private toggleTilesDisabled(tiles: HTMLButtonElement[], emptyTile: HTMLDivElement) {
    const emptyTileArea = this.getTileArea(emptyTile);
    tiles.forEach(tile => {
      const tileArea = this.getTileArea(tile);

      // Check if that areaKey has the tiles area in it's values
      const isDisabled = !this.AREA_KEYS[emptyTileArea]?.includes(tileArea);
      tile.disabled = isDisabled;
    });
  }

  private checkComplete() {
    const tiles = this.allTiles;

    // Get all the current tile area values
    const currentTileAreaValues = tiles
      .map(tile => this.getTileArea(tile))
      .toString();

    const isCompleted = currentTileAreaValues === Object.keys(this.AREA_KEYS).toString();
    if (!isCompleted) {
      return;
    }

    setTimeout(() => {
      this.completed.next();
    }, 500);
  }

  private shuffle() {
    // Inversion calculator
    const inversionCount = array => {
      // Using the reduce function to run through all items in the array
      // Each item in the array is checked against everything before it
      // This will return a new array with each intance of an item appearing before it's original predecessor
      // tslint:disable-next-line:no-shadowed-variable
      return array.reduce((accumulator: any, current: number, index: any, array: any[]) => {
        return array
          .slice(index)
          .filter(item => {
            return item < current;
          })
          .map(item => {
            return [current, item];
          })
          .concat(accumulator);
      }, []).length;
    };

    // Randomise tiles
    const shuffledKeys = keys => Object.keys(keys).sort(() => .5 - Math.random());

    setTimeout(() => {
      // Begin with our in order area keys
      let startingAreas = Object.keys(this.AREA_KEYS);

      // Use the inversion function to check if the keys will be solveable or not shuffled
      // Shuffle the keys until they are solvable
      while (inversionCount(startingAreas) % 2 === 1 || inversionCount(startingAreas) === 0) {
        startingAreas = shuffledKeys(this.AREA_KEYS);
      }

      // Apply shuffled areas
      this.allTiles.map((tile, index) => {
        tile.style.setProperty('--area', startingAreas[index]);
      });

      // Initial shuffle animation
      this.gridAnimation();

      // Unlock and lock tiles
      this.toggleTilesDisabled(
        this.allTiles.filter(v => v.nodeName === 'BUTTON') as HTMLButtonElement[],
        this.allTiles.find(v => v.nodeName === 'DIV') as HTMLDivElement,
      );
    }, 0);
  }
}

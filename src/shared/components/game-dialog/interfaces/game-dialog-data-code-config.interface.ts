import { GameDialogDataField } from './game-dialog-data-field.interface';

export interface GameDialogDataCodeConfig {
  code?: string;
  codeLength?: number;
  inputType?: string;
  isCodeHidden?: boolean;
  isNonDigitsCode?: boolean;
  codeChanged?: (code: string, field: GameDialogDataField) => void;
  codeCompleted?: (code: string, field: GameDialogDataField) => void;
}

import { GameDialogDataField } from './game-dialog-data-field.interface';

export interface GameDialogData {
  panelClass?: string | string[];
  fields?: GameDialogDataField[];
  hideClose?: boolean;
  iconCloseUrl?: string;
}

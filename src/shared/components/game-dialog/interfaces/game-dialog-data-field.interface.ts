import { MatDialogRef } from '@angular/material/dialog';
import { GameDialogDataFieldType } from '../enums/game-dialog-data-field-type.enum';
import { GameDialogComponent } from '../game-dialog.component';
import { GameDialogDataCodeConfig } from './game-dialog-data-code-config.interface';
import { GameDialogData } from './game-dialog-data.interface';

export interface GameDialogDataField {
  type: GameDialogDataFieldType;
  value?: any;
  id?: string;
  clazz?: string;
  disabled?: boolean;
  hidden?: boolean;
  fields?: GameDialogDataField[];
  codeConfig?: GameDialogDataCodeConfig;
  onClick?: (dialogRef: MatDialogRef<GameDialogComponent>, field: GameDialogDataField, data: GameDialogData) => void;
  onCompleted?: (value: any, dialogRef: MatDialogRef<GameDialogComponent>, field: GameDialogDataField, data: GameDialogData) => void;
}

export interface GameDialogObjectEvent {
  id: string;
  mousedown?: (ev: MouseEvent) => void;
  mouseenter?: (ev: MouseEvent) => void;
  mouseleave?: (ev: MouseEvent) => void;
}

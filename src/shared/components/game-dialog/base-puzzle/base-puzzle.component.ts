import { GameConstants } from '../../../constants/game.constant';
import { BaseComponent } from '../../base-component/base.component';
import { GameDialogData } from '../interfaces/game-dialog-data.interface';
import { GameDialogObjectEvent } from '../interfaces/game-dialog-object-event.interface';

export abstract class BasePuzzleComponent extends BaseComponent {
  protected hideElementsById(ids: string[]) {
    ids.forEach((id) => this.hideElementById(id));
  }

  protected hideElementById(id: string) {
    const el = document.getElementById(id);
    this.hideElement(el);
  }

  protected showElementsById(ids: string[]) {
    ids.forEach((id) => this.showElementById(id));
  }

  protected showElementById(id: string) {
    const el = document.getElementById(id);
    this.showElement(el);
  }

  protected hideElement(el: Element) {
    if (!el) {
      return;
    }
    el.classList.add(GameConstants.DISPLAY_NONE_CLASS);
  }

  protected showElement(el: Element) {
    if (!el) {
      return;
    }
    el.classList.remove(GameConstants.DISPLAY_NONE_CLASS);
  }

  protected assignObjectEvent(objects: GameDialogObjectEvent[]) {
    objects.forEach((v) => {
      const el = (document.getElementById(v.id) as Element) as SVGElement;
      if (!el) {
        return;
      }
      if (v.mousedown) {
        el.classList.add(GameConstants.CLICKABLE_CLASS);
        el.onmousedown = v.mousedown;
      }
      if (v.mouseenter) {
        el.onmouseenter = v.mouseenter;
      }
      if (v.mouseleave) {
        el.onmouseleave = v.mouseleave;
      }
    });
  }

  protected clearObjectEvent(ids: string[]) {
    ids.forEach((id) => {
      const el = (document.getElementById(id) as Element) as SVGElement;
      if (!el) {
        return;
      }
      el.classList.remove(GameConstants.CLICKABLE_CLASS);
      el.onmousedown = null;
    });
  }

  protected getDialogDataSuccess(): GameDialogData {
    return null;
  }

  protected getDialogDataFailed(): GameDialogData {
    return null;
  }
}

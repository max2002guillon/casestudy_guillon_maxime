export enum GameDialogDataFieldType {
  ROW,
  TITLE,
  TEXT,
  ERROR,
  IMAGE,
  BUTTON,
  MEDIA,
  CODE_INPUT,
}

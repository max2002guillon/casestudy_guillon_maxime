import { Component, EventEmitter, Inject, Output, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseComponent } from '../base-component/base.component';
import { GameDialogDataFieldType } from './enums/game-dialog-data-field-type.enum';
import { GameDialogDataField } from './interfaces/game-dialog-data-field.interface';
import { GameDialogData } from './interfaces/game-dialog-data.interface';

@Component({
  selector: 'app-game-dialog',
  templateUrl: './game-dialog.component.html',
  styleUrls: ['./game-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GameDialogComponent extends BaseComponent {

  @Output() customClose: EventEmitter<any> = new EventEmitter();

  public readonly gameDialogDataFieldType = GameDialogDataFieldType;

  constructor(
    public readonly dialogRef: MatDialogRef<GameDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public readonly data: GameDialogData,
  ) {
    super();
  }

  public close() {
    this.customClose.emit();
    this.dialogRef.close();
  }

  public onClick(field: GameDialogDataField) {
    if (!field.onClick) {
      return;
    }
    field.onClick(this.dialogRef, field, this.data);
  }

  public onCompleted(ev: any, field: GameDialogDataField) {
    if (!field.onCompleted) {
      return;
    }
    field.onCompleted(ev, this.dialogRef, field, this.data);
  }

  onInit(): void {
    this.dialogRef.addPanelClass(this.data?.panelClass);
  }

  onAfterViewInit() {
    const inputs = Array.from(
      document.querySelectorAll('code-input input')
    ) as HTMLInputElement[];
    inputs.forEach((input) => {
      input.autocapitalize = 'off';
    });
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-inventory-dialog',
  templateUrl: './inventory-dialog.component.html',
  styleUrls: ['./inventory-dialog.component.scss']
})
export class InventoryDialogComponent extends BaseComponent {

  public imagesUrl = [];
  public maxImage = 4;

  constructor(
    public readonly dialogRef: MatDialogRef<InventoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public readonly dialogData: any,
  ) {
    super();
  }

  onInit(): void {
    this.imagesUrl = this.dialogData?.imagesUrl;
    this.maxImage = this.dialogData?.maxImage;
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}

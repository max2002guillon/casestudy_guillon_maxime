import { Component } from '@angular/core';
import { AssetService } from '../../assets/services/asset.service';
import { AssetKeyConstant } from '../../constants/asset-key.constant';
import { GameSoundService } from '../../services/audio/game-sound.service';
import { LayoutService } from '../../services/layout.service';
import { GameBaseComponent } from '../game-base/game-base.component';

@Component({
  selector: 'app-game-sound',
  templateUrl: './game-sound.component.html',
  styleUrls: ['./game-sound.component.scss'],
})
export class GameSoundComponent extends GameBaseComponent {
  public isMove: boolean;
  public isMobile = false;
  public get iconSrc(): string {
    return this.gameSoundService.iconSrc;
  }

  constructor(
    private readonly gameSoundService: GameSoundService,
    private readonly assetService: AssetService,
    private readonly layoutService: LayoutService,
  ) {
    super();
  }

  public toggleMute() {
    this.gameSoundService.toggleMute();
  }

  onInit() {
    this.initSubscriptions();
    this.gameSoundService.setMuteIcon(this.assetService.getUrl(AssetKeyConstant.PERMANENT_ICON_MUTE));
    this.gameSoundService.setUnmuteIcon(this.assetService.getUrl(AssetKeyConstant.PERMANENT_ICON_UNMUTE));

    this.subscribe(this.layoutService.isMobile(), isMobile => this.isMobile = isMobile);
  }

  private initSubscriptions() {
    this.subscribeVisibilityObservable(this.gameSoundService.getVisibilityObservable());
  }
}

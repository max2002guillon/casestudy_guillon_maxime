import { animate, style, transition, trigger } from '@angular/animations';
import { CdkDragDrop, CdkDragEnd, CdkDragStart } from '@angular/cdk/drag-drop';
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';
import { GameInventoryItem } from '../../interfaces/game-inventory-item.interface';
import { GameInventoryService } from '../../services/game-inventory.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { GameDialogComponent } from '../game-dialog/game-dialog.component';

@Component({
  selector: 'app-game-inventory-item-container',
  templateUrl: './game-inventory-item-container.component.html',
  styleUrls: ['./game-inventory-item-container.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('1s ease-out',
              style({ opacity: 1 }))
          ]
        )
      ]
    )
  ]
})
export class GameInventoryItemContainerComponent extends GameBaseComponent {
  @ViewChild(DragScrollComponent) dragScrollComponent: DragScrollComponent;

  public backgroundImageUrl: string;
  public readonly NUMB_BOX = 3;

  public get items(): GameInventoryItem[] {
    return this.gameInventoryService.items;
  }

  public get listConnectId(): string[] {
    return this.gameInventoryService.listConnectId;
  }

  private readonly DRAG_ENABLE_CLASS = 'drag-enable';

  constructor(
    private readonly dialog: MatDialog,
    private readonly assetService: AssetService,
    private readonly gameInventoryService: GameInventoryService,
  ) {
    super();
  }

  onInit(): void {
    this.backgroundImageUrl = `url(${this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_INVENTORY_COLLECTOR_OPEN)})`;
  }

  public moveLeft() {
    const el = this.dragScrollComponent._contentRef.nativeElement;
    this.gameInventoryService.scrollToPreviousItem(el);
  }

  public moveRight() {
    const el = this.dragScrollComponent._contentRef.nativeElement;
    this.gameInventoryService.scrollToNextItem(el);
  }

  public onClick(item: GameInventoryItem) {
    if (!item.isActive) {
      return;
    }
    if (item.dialogData) {
      this.dialog.open(GameDialogComponent, { data: item.dialogData });
    }
    if (item.onClick) {
      item.onClick();
    }
  }

  public onDragStarted($event: CdkDragStart, item: GameInventoryItem) {
    const target = $event?.source?.element?.nativeElement as HTMLElement;
    if (!target?.classList.contains(this.DRAG_ENABLE_CLASS)) {
      target.classList.add(this.DRAG_ENABLE_CLASS);
    }

    if (item.onDragStarted) {
      item.onDragStarted($event);
    }
  }

  public onDragEnded($event: CdkDragEnd, item: GameInventoryItem) {
    if (item.onDragEnded) {
      item.onDragEnded($event);
    }
  }

  public drop($event: CdkDragDrop<any[]>) {
    const target = $event?.item?.element?.nativeElement as HTMLElement;
    if (target?.classList.contains(this.DRAG_ENABLE_CLASS)) {
      target.classList.remove(this.DRAG_ENABLE_CLASS);
    }
  }
}

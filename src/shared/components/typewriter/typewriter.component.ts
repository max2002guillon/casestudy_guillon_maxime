import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { takeWhile, tap } from 'rxjs/operators';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-typewriter',
  templateUrl: './typewriter.component.html',
  styleUrls: ['./typewriter.component.scss'],
})
export class TypewriterComponent extends BaseComponent implements OnChanges {
  @Input() content: string;
  @Input() delay = 100;
  @Input() speed = 10;

  @Output() completed = new EventEmitter();

  public displayContent = '';

  private writerSubscription: Subscription;

  ngOnChanges(changes: SimpleChanges) {
    if (this.content) {
      this.execWriter();
    }
  }

  private execWriter() {
    if (this.writerSubscription) {
      this.writerSubscription.unsubscribe();
    }
    this.displayContent = '';
    const content = this.content;
    const writerObservable = timer(this.delay, this.speed)
      .pipe(
        takeWhile(v => content.length > v),
        tap(v => this.displayContent += content[v]),
        tap(() => {
          if (this.displayContent.length === content.length) {
            setTimeout(() => {
              this.completed.next();
            });
          }
        }),
      );
    this.writerSubscription = writerObservable.subscribe();
    this.addSubscription(this.writerSubscription);
  }
}

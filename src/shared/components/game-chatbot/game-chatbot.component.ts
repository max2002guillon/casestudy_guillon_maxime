import { Component, ElementRef, ViewChild } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { Observable, Subscription, timer } from 'rxjs';
import { debounceTime, finalize, take, takeWhile, tap } from 'rxjs/operators';
import { AssetKeyConstant } from 'src/app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { GameChatBotService } from './../../services/game-chatbot.service';
import { LayoutService } from './../../services/layout.service';

@Component({
  selector: 'app-game-chatbot',
  templateUrl: './game-chatbot.component.html',
  styleUrls: ['./game-chatbot.component.scss'],
})
export class GameChatBotComponent extends GameBaseComponent {
  @ViewChild('content', { static: true }) contentElementRef: ElementRef;

  public isDialogVisible = false;
  public isContentReady = false;
  public isMobile = false;

  public imgBtnCloseUrl: string;
  public imgDefaultUrl: string;
  public imgClickedUrl: string;

  public contentObs: Observable<string>;
  public displayContent: string;

  // public iconChatBotUrl: AnimationOptions;
  public iconChatBotUrl: string | AnimationOptions;
  public ieImgUrl: string;
  public borderImageSource: string;

  private typingSubscription: Subscription;

  private readonly TYPING_DELAY = 100;
  private readonly TYPING_INTERVAL = 40;
  private readonly NON_BREAKING_SPACE = '&nbsp;';
  private readonly WAIT_TIME_ANIMATION = 1000;

  private get contentElement(): HTMLElement {
    return this.contentElementRef?.nativeElement;
  }

  constructor(
    private readonly assetService: AssetService,
    private readonly gameChatBotService: GameChatBotService,
    private readonly layoutService: LayoutService
  ) {
    super();
  }

  public get btnActionText() {
    return this.gameChatBotService.btnActionText$;
  }

  public get btnActionEvent() {
    return this.gameChatBotService.btnActionEvent;
  }

  public onClick(): void {
    this.isDialogVisible = false;
    if (this.gameChatBotService.actionClose) {
      this.gameChatBotService.actionClose();
      this.gameChatBotService.actionClose = null;
    }
  }

  public onMouseOver(): void {
    this.imgBtnCloseUrl = this.imgClickedUrl;
  }

  public onMouseLeave(): void {
    this.imgBtnCloseUrl = this.imgDefaultUrl;
  }

  onInit() {
    this.initSubscriptions();

    this.subscribe(this.layoutService.isMobile(), isMobile => this.isMobile = isMobile);
  }

  private configClose(): void {
    this.imgDefaultUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_CLOSE) || undefined;
    this.imgClickedUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_CLOSE_CLICKED);
    this.imgBtnCloseUrl = this.imgDefaultUrl;
  }

  private initSubscriptions() {
    this.subscribeVisibilityObservable(
      this.gameChatBotService.getVisibilityObservable()
    );

    this.subscribe(
      this.gameChatBotService.getDialogContentObservable().pipe(
        tap(() => {
          this.displayContent = '';
          this.configClose();
          this.isDialogVisible = true;
          this.iconChatBotUrl = null;
          this.initAssets();
        }),
        debounceTime(this.WAIT_TIME_ANIMATION)
      ),
      (contentObs) => {
        this.contentObs = contentObs;
        this.runTypingContent();
      }
    );
  }

  private findIndexesOfSubstring(source: string, find: string) {
    if (!source || !find) {
      return [];
    }
    const result = [];
    for (let i = 0; i < source.length; ++i) {
      if (source.substring(i, i + find.length) === find) {
        result.push(i);
      }
    }
    return result;
  }

  private runTypingContent() {
    this.isContentReady = false;

    if (!this.isVisible || !this.isDialogVisible || !this.contentObs) {
      return;
    }

    this.displayContent = '';
    this.contentObs.pipe(take(1)).subscribe((content) => {
      if (this.typingSubscription) {
        this.typingSubscription.unsubscribe();
      }
      const indexes = this.findIndexesOfSubstring(content, this.NON_BREAKING_SPACE);
      let currentIndex = 0;
      this.typingSubscription = timer(this.TYPING_DELAY, this.TYPING_INTERVAL)
        .pipe(
          takeWhile(() => this.isVisible),
          takeWhile(() => this.isDialogVisible),
          takeWhile(
            (v) => content && v < content.replace(/&nbsp;/g, '*').length
          ),
          tap(() => {
            if (indexes.find((i) => i === currentIndex)) {
              this.displayContent += this.NON_BREAKING_SPACE;
              currentIndex += this.NON_BREAKING_SPACE.length;
            } else {
              this.displayContent += content[currentIndex];
              currentIndex++;
            }
          }),
          tap(() => {
            this.contentElement?.scroll({
              left: 0,
              top: Number.MAX_SAFE_INTEGER,
              behavior: 'smooth',
            });
          }),
          finalize(() => {
            this.isContentReady = true;
          })
        )
        .subscribe();
    });
  }

  private initAssets(): void {
    this.iconChatBotUrl = {
      loop: false,
      path: 'assets/animations/permanents/chatbot/chatbot.json'
    };
  }
}

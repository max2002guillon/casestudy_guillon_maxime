import { trigger, transition, style, animate } from '@angular/animations';
import { ChangeDetectorRef, Component } from '@angular/core';
import { timer } from 'rxjs';
import { GameBaseComponent } from '../game-base/game-base.component';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { GameKeyService } from './../../services/game-key.service';
import { AssetService } from '../../assets/services/asset.service';

@Component({
  selector: 'app-game-key',
  templateUrl: './game-key.component.html',
  styleUrls: ['./game-key.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('1.5s ease-out',
              style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1 }),
            animate('0.5s ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class GameKeyComponent extends GameBaseComponent {
  public numbOfKeyFounded = 0;
  public keyUrl: string;
  public isKeyCollected = false;

  private readonly TIMER_DELAY_COLLECTED = 2 * 1000;

  constructor(
    private readonly gameKeyService: GameKeyService,
    private readonly assetService: AssetService,
    private readonly cdRef: ChangeDetectorRef,
  ) {
    super();
  }

  onInit() {
    this.keyUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_KEY);
    this.gameKeyService.init();
  }

  onAfterViewInit(): void {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.subscribeVisibilityObservable(this.gameKeyService.getVisibilityObservable());

    this.subscribe(this.gameKeyService.numbOfKeyObservable, (result) => {
      this.numbOfKeyFounded = result;
      if (result > 0) {
        this.isKeyCollected = true;

        this.subscribe(timer(this.TIMER_DELAY_COLLECTED), () => {
          this.isKeyCollected = false;
        });
      }

      this.cdRef.detectChanges();
    }
    );
  }
}

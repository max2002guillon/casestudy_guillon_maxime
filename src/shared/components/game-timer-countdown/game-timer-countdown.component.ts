import { Component } from '@angular/core';
import { AssetService } from '../../assets/services/asset.service';
import { AssetKeyConstant } from '../../constants/asset-key.constant';
import { GameTimerCountDownService } from '../../services/game-timer-countdown.service';
import { GameBaseComponent } from '../game-base/game-base.component';

@Component({
  selector: 'app-game-timer-countdown',
  templateUrl: './game-timer-countdown.component.html',
  styleUrls: ['./game-timer-countdown.component.scss'],
})
export class GameTimerCountDownComponent extends GameBaseComponent {
  public minuteValue: string;
  public secondValue: string;
  public backgroundImage: string;

  constructor(
    private readonly gameTimerCountDownService: GameTimerCountDownService,
    private readonly assetService: AssetService
  ) {
    super();
  }

  onInit() {
    const imageUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_TIMER);
    this.backgroundImage = `url(${imageUrl})`;
    this.setTimerValue(0);
  }

  onAfterViewInit() {
    this.initSubscriptions();
  }

  private initSubscriptions() {
    this.subscribeVisibilityObservable(this.gameTimerCountDownService.getVisibilityObservable());

    this.subscribe(
      this.gameTimerCountDownService.getTimerTickObservable(),
      milliseconds => {
        this.setTimerValue(milliseconds);
      },
    );
  }

  private setTimerValue(milliseconds: number) {
    this.secondValue = String(Math.floor(milliseconds / 1000) % 60).padStart(2, '0');
    this.minuteValue = String(Math.floor(milliseconds / 1000 / 60) % 60).padStart(2, '0');
  }
}

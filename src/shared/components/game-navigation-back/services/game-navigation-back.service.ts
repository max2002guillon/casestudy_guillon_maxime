import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GameComponentService } from '../../../services/game-component.service';

@Injectable()
export class GameNavigationBackService extends GameComponentService {
  private isShowNavigationBackSubject = new Subject();

  public actionBack: () => any;

  constructor() {
    super();
  }

  public setStateNavigationBack(state: boolean): void {
    this.isShowNavigationBackSubject.next(state);
  }

  public getStateNavigationBack(): Observable<any> {
    return this.isShowNavigationBackSubject.asObservable();
  }
}

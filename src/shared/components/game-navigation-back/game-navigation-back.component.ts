import { ChangeDetectorRef, Component } from '@angular/core';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';
import { LayoutService } from '../../services/layout.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { GameNavigationBackService } from './services/game-navigation-back.service';

@Component({
  selector: 'app-game-navigation-back',
  templateUrl: './game-navigation-back.component.html',
  styleUrls: ['./game-navigation-back.component.scss']
})
export class GameNavigationBackComponent extends GameBaseComponent {
  public imgUrl: string;
  public backUrl: string;
  public backUrlClicked: string;
  public isShowNavigationBack = false;
  public isMobile = false;
  public isMulti = false;

  constructor(
    private readonly gameNavigationBackService: GameNavigationBackService,
    private readonly assetService: AssetService,
    private readonly cd: ChangeDetectorRef,
    private readonly layoutService: LayoutService,
  ) {
    super();
  }

  public onClick(): void {
    this.gameNavigationBackService.setStateNavigationBack(false);
    this.gameNavigationBackService.actionBack();
  }

  onInit(): void {
    this.backUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_NAVIGATION_BACK) || undefined;
    this.imgUrl = this.backUrl;
    this.initSubscriptions();

    this.subscribe(this.layoutService.isMobile(), isMobile => this.isMobile = isMobile);
  }

  onMouseOver() {
    this.imgUrl = this.backUrlClicked;
  }

  onMouseLeave() {
    this.imgUrl = this.backUrl;
  }

  private initSubscriptions(): void {
    this.subscribe(this.gameNavigationBackService.getStateNavigationBack(), (state: boolean) => {
      this.isShowNavigationBack = state;
      if (state) {
        this.imgUrl = this.backUrl;
      }
      this.cd.detectChanges();
    });

    this.subscribeVisibilityObservable(this.gameNavigationBackService.getVisibilityObservable());
  }
}

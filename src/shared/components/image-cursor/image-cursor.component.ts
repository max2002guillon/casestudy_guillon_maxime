import { Component, HostListener, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-image-cursor',
  templateUrl: './image-cursor.component.html',
  styleUrls: ['./image-cursor.component.scss'],
})
export class ImageCursorComponent extends BaseComponent implements OnChanges {
  @Input() imageSrc: string;
  @Input() offsetX = 0;
  @Input() offsetY = 0;

  public top: string;
  public left: string;
  public imgOffsetX: string;
  public imgOffsetY: string;

  ngOnChanges(changes: SimpleChanges): void {
    this.imgOffsetX = -this.offsetX + 'px';
    this.imgOffsetY = -this.offsetY + 'px';
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(ev: MouseEvent) {
    const top = ev.pageY;
    const left = ev.pageX;
    this.top = top + 'px';
    this.left = left + 'px';
  }
}

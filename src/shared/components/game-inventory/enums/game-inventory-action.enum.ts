export enum GameInventoryAction {
  OPEN,
  CLOSE,
  CLOSE_LOCK
}

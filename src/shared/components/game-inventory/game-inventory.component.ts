import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';
import { GameInventoryService } from '../../services/game-inventory.service';
import { LayoutService } from '../../services/layout.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { GameInventoryAction } from './enums/game-inventory-action.enum';

@Component({
  selector: 'app-game-inventory',
  templateUrl: './game-inventory.component.html',
  styleUrls: ['./game-inventory.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('0.5s ease-out',
              style({ opacity: 1 }))
          ]
        ),
      ]
    )
  ]
})
export class GameInventoryComponent extends GameBaseComponent {
  @ViewChild('inventory') inventoryElementRef: ElementRef;

  public imageBagUrl: string;

  public classByScene: string;
  public isOpen = false;
  public isMobile = false;

  public get label() {
    return this.gameInventoryService.label;
  }

  constructor(
    public readonly gameInventoryService: GameInventoryService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly assetService: AssetService,
    private readonly layoutService: LayoutService,
  ) {
    super();
  }

  public toggle(isOpen?): void {
    if (!this.inventoryElementRef) {
      return;
    }
    this.isOpen = isOpen !== undefined ? isOpen : !this.isOpen;
  }

  onInit() {
    this.subscribe(this.layoutService.isMobile(), isMobile => {
      this.isMobile = isMobile;
    });
  }

  onAfterViewInit() {
    this.initSubscriptions();
    this.showInventoryByGameIndex();
  }

  private initSubscriptions(): void {
    this.subscribeVisibilityObservable(this.gameInventoryService.getVisibilityObservable());

    this.subscribe(
      this.gameInventoryService.actionObservable,
      action => {
        switch (action) {
          case GameInventoryAction.OPEN:
            this.toggle(true);
            return;
          case GameInventoryAction.CLOSE:
            this.toggle(false);
            return;
        }
      },
    );
  }

  private showInventoryByGameIndex(): void {
    this.imageBagUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_INVENTORY_COLLECTOR_CLOSED);
    this.cdRef.detectChanges();
  }
}

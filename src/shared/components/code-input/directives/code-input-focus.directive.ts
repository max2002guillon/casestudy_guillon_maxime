import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[appCodeInputFocus]' })
export class CodeInputFocusDirective implements AfterViewInit {
  private codeInput: HTMLElement;
  private inputs: HTMLInputElement[];

  constructor(el: ElementRef) {
    this.codeInput = el.nativeElement;
  }

  ngAfterViewInit(): void {
    this.inputs = Array.from(this.codeInput.querySelectorAll('input'));
    this.inputs?.forEach((currentInput, i) => {
      if (i === 0) {
        return;
      }
      currentInput.onfocus = () => {
        const currentValue = currentInput.value;
        if (currentValue) {
          return;
        }
        const prevInput = this.inputs[i - 1];
        const prevValue = prevInput.value;
        if (prevValue) {
          return;
        }
        prevInput.focus();
      };
    });
  }
}

import { NgModule } from "@angular/core";
import { CodeInputComponent } from "./code-input.component";
import { CodeInputFocusDirective } from "./directives/code-input-focus.directive";

@NgModule({
  declarations: [CodeInputFocusDirective, CodeInputComponent],
  exports: [CodeInputFocusDirective, CodeInputComponent]
})
export class CodeInputModule {}
import { ChangeDetectorRef, Component, HostBinding } from '@angular/core';
import { GameBoardService } from '../../services/game-board.service';
import { BaseComponent } from '../base-component/base.component';

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss'],
})
export class GameBoardComponent extends BaseComponent {
  @HostBinding('style.height') height = '100%';

  public boardlBackgroundImageUrl: string;

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly gameBoardService: GameBoardService,
  ) {
    super();
  }

  onInit() {
    this.initSubscriptions();
  }

  private initSubscriptions() {
    this.subscribe(this.gameBoardService.getBoardBackgroundImageUrlObservable(), value => {
      this.boardlBackgroundImageUrl = value;
      this.cdr.detectChanges();
    });
  }
}

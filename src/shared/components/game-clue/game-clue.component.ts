import { ChangeDetectorRef, Component } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { GameClueService } from './services/game-clue.service';

@Component({
  selector: 'app-game-clue',
  templateUrl: './game-clue.component.html',
  styleUrls: ['./game-clue.component.scss']
})
export class GameClueComponent extends GameBaseComponent {

  public imageUrl: string;
  public currentGameIndex = 1;
  public isOpen = false;
  public message: Observable<any>;

  constructor(
    private readonly gameClueService: GameClueService,
    // private readonly userGamingProgressService: UserGamingProgressService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly assetService: AssetService
  ) {
    super();
  }

  onAfterViewInit(): void {
    this.initSubscriptions();
    this.showClueByGameIndex();
  }

  public onClick(): void {
    this.isOpen = true;
    this.message = this.gameClueService.getClueMessage();

    timer(5000).subscribe(() => this.isOpen = false);

    // this.userGamingProgressService.increaseUserGamingClickClueTimes(this.gameClueService.getClueScene());
  }

  private initSubscriptions(): void {
    this.subscribeVisibilityObservable(this.gameClueService.getVisibilityObservable());

    this.subscribe(
      this.gameClueService.isShowClueMessage$,
      (isShowClue) => {
        this.isOpen = isShowClue;
      }
    );
  }

  private showClueByGameIndex(): void {
    const imageUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_CLUE);
    this.imageUrl = imageUrl;
    this.cdRef.detectChanges();
  }
}

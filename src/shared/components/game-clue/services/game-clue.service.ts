import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { GameScene } from '../../../../app/modules/game/enums/game-sceen.enum';
import { GameComponentService } from '../../../services/game-component.service';

@Injectable({ providedIn: 'root' })
export class GameClueService extends GameComponentService {
  private isShowClue = new Subject<boolean>();
  private currentMessage: Observable<any>;
  private currentScene: GameScene;

  constructor(
    private readonly translateService: TranslateService
  ) {
    super();
  }

  public setClueMessage(messageKey, scene: GameScene): void {
    this.currentMessage = this.translateService.get(messageKey);
    this.currentScene = scene;
  }

  public getClueMessage(): Observable<any> {
    return this.currentMessage;
  }

  public getClueScene() {
    return this.currentScene;
  }

  public showClueMessage(): void {
    this.isShowClue.next(true);
  }

  public get isShowClueMessage$(): Observable<boolean> {
    return this.isShowClue.asObservable();
  }
}

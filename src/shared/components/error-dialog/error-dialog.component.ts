import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AssetService } from '../../assets/services/asset.service';
import { AssetKeyConstant } from '../../constants/asset-key.constant';
import { ResponsiveLayoutService } from '../../services/responsive-layout.service';
import { BaseComponent } from '../base-component/base.component';
@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})

export class ErrorDialogComponent extends BaseComponent {
  public textError = '';
  public borderUrl = 'assets/icons/enlimunire.svg';

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private readonly data: any,
    private readonly dialogRef: MatDialogRef<ErrorDialogComponent>,
    private readonly responsiveLayoutService: ResponsiveLayoutService,
    private readonly assetService: AssetService,
  ) {
    super();
  }

  onReturn() {
    if (!this.data.onReturn) {
      return;
    }
    this.dialogRef.close();
    this.data.onReturn();
  }

  onInit() {
    if (this.data) {
      this.textError = this.data.textError;
    }

    this.subscribe(this.responsiveLayoutService.isMobileLayout(), async (isMobile) => {
      this.borderUrl = this.assetService.getUrl(
        isMobile ? AssetKeyConstant.BORDER_DIALOG_CODE_WRONG_MOBILE_URL : AssetKeyConstant.BORDER_DIALOG_CODE_WRONG_URL);
    });
  }


}

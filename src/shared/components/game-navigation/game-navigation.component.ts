import { Component } from '@angular/core';
import { GameBoardService } from '../../services/game-board.service';
import { GameNavigationService } from '../../services/game-navigation.service';
import { GameBaseComponent } from '../game-base/game-base.component';
import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { AssetService } from '../../assets/services/asset.service';

@Component({
  selector: 'app-game-navigation',
  templateUrl: './game-navigation.component.html',
  styleUrls: ['./game-navigation.component.scss'],
})
export class GameNavigationComponent extends GameBaseComponent {
  public navArrowUrl: string;

  public get isLeftNavigatorVisible(): boolean {
    const boundary = 0;
    const isVisible = this.isVisible && this.gameNavigationService.scrollLeft > boundary;
    return isVisible;
  }

  public get isRightNavigatorVisible(): boolean {
    const boundary = this.gameBoardService.gameDragScrollContent.scrollWidth - window.innerWidth;
    const isVisible = this.isVisible && this.gameNavigationService.scrollLeft < boundary;
    return isVisible;
  }

  constructor(
    private readonly gameBoardService: GameBoardService,
    private readonly gameNavigationService: GameNavigationService,
    private readonly assetService: AssetService,
  ) {
    super();
  }

  public scrollLeft(ev: MouseEvent) {
    if (!this.isMainButton(ev)) {
      return;
    }
    this.gameNavigationService.scrollLeftByPercent();
  }

  public scrollRight(ev: MouseEvent) {
    if (!this.isMainButton(ev)) {
      return;
    }
    this.gameNavigationService.scrollRightByPercent();
  }

  onInit() {
    this.navArrowUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_NAVIGATION);

    this.gameNavigationService.init();
    this.initSubscriptions();
  }

  private initSubscriptions() {
    this.subscribeVisibilityObservable(this.gameNavigationService.getVisibilityObservable());
    this.subscribeFreezedObservable(this.gameNavigationService.getFreezedObservable());
  }

  private isMainButton(ev: MouseEvent) {
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
    const isMainButton = ev.button === 0;
    return isMainButton;
  }
}

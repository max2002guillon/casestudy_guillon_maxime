import { AssetKeyConstant } from '../../../app/constants/asset-key.constant';
import { Component } from '@angular/core';
import { GameBaseComponent } from '../game-base/game-base.component';
import { AssetService } from '../../assets/services/asset.service';

@Component({
  selector: 'app-game-loading-dialog',
  templateUrl: './game-loading-dialog.component.html',
  styleUrls: ['./game-loading-dialog.component.scss'],
})
export class GameLoadingDialogComponent extends GameBaseComponent { 
  public loadingUrl: string;

  constructor(
    private readonly assetService: AssetService,
  ) {
    super();
  }

  onInit() {
    this.loadingUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_LOADING);
  }

}

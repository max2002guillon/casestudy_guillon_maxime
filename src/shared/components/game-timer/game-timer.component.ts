import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { AssetService } from '../../assets/services/asset.service';
import { AssetKeyConstant } from '../../constants/asset-key.constant';
import { GameTimerService } from '../../services/game-timer.service';
import { LayoutService } from '../../services/layout.service';
import { GameBaseComponent } from '../game-base/game-base.component';

@Component({
  selector: 'app-game-timer',
  templateUrl: './game-timer.component.html',
  styleUrls: ['./game-timer.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('1s ease-out',
              style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1 }),
            animate('1s ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class GameTimerComponent extends GameBaseComponent {
  public hourValue: string;
  public minuteValue: string;
  public secondValue: string;
  public backgroundImage: string;
  public isPenaltyVisible = false;
  public isMobile = false;

  constructor(
    private readonly layoutService: LayoutService,
    private readonly assetService: AssetService,
    private readonly gameTimerService: GameTimerService,
  ) {
    super();
  }

  onInit() {
    this.initData();
    this.initSubscriptions();
  }

  private initData() {
    const imageUrl = this.assetService.getUrl(AssetKeyConstant.PERMANENT_ELEMENT_TIMER);
    this.backgroundImage = `url(${imageUrl})`;
    this.setTimerValue(0);
  }

  private initSubscriptions() {
    this.subscribeVisibilityObservable(this.gameTimerService.getVisibilityObservable());

    this.subscribe(this.layoutService.isMobile(), isMobile => {
      this.isMobile = isMobile;
    });

    this.subscribe(
      this.gameTimerService.getTimerTickObservable(),
      milliseconds => {
        this.setTimerValue(milliseconds);
      },
    );

    this.subscribe(
      this.gameTimerService.getTimerPenaltyVisibleObservable(),
      isVisible => this.isPenaltyVisible = isVisible
    );
  }

  private setTimerValue(milliseconds: number) {
    milliseconds = milliseconds || 0;
    this.secondValue = this.getTimerNumberValue(Math.floor(milliseconds / 1000) % 60);
    this.minuteValue = this.getTimerNumberValue(Math.floor(milliseconds / 1000 / 60) % 60);
    this.hourValue = this.getTimerNumberValue(Math.floor(milliseconds / 1000 / 60 / 60) % 24);
  }

  private getTimerNumberValue(n: number) {
    const s = String(n).padStart(2, '0');
    return s;
  }
}

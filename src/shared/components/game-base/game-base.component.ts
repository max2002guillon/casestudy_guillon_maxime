import { Observable } from 'rxjs';
import { BaseComponent } from '../base-component/base.component';

export abstract class GameBaseComponent extends BaseComponent {
  public isVisible = true;
  public isFreezed = false;

  protected subscribeVisibilityObservable(visibilityObservable: Observable<boolean>) {
    this.subscribe(
      visibilityObservable,
      isVisible => {
        this.isVisible = isVisible;
      },
    );
  }

  protected subscribeFreezedObservable(freezedObservable: Observable<boolean>) {
    this.subscribe(
      freezedObservable,
      isFreezed => {
        this.isFreezed = isFreezed;
      },
    );
  }
}

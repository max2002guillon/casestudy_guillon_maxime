import { BehaviorSubject, Observable } from 'rxjs';
import { Subscribable } from '../components/base-component/subscribable';

export abstract class GameComponentService extends Subscribable {
  protected visibilitySubject = new BehaviorSubject<boolean>(true);
  protected freezedSubject = new BehaviorSubject<boolean>(false);

  public getVisibilityObservable(): Observable<boolean> {
    return this.visibilitySubject.asObservable();
  }

  public setVisibility(isVisible: boolean) {
    this.visibilitySubject.next(isVisible);
  }

  public getFreezedObservable(): Observable<boolean> {
    return this.freezedSubject.asObservable();
  }

  public setFreezed(isFreezed: boolean) {
    this.freezedSubject.next(isFreezed);
  }
}

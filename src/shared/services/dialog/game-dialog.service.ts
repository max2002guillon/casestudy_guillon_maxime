import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { GameDialogComponent } from '../../components/game-dialog/game-dialog.component';
import { GameDialogData } from '../../components/game-dialog/interfaces/game-dialog-data.interface';
import { DynamicDialogRef } from '../../modules/dynamic-dialog/services/dynamic-dialog-ref';
import { DynamicDialogService } from '../../modules/dynamic-dialog/services/dynamic-dialog.service';
import { DynamicDialogConfig } from '../../modules/dynamic-dialog/types/dynamic-dialog.type';

@Injectable()
export class GameDialogService {
  constructor(private readonly dynamicDialogService: DynamicDialogService) { }

  public closeAll() {
    // this.dynamicDialogService.closeAll();
  }

  public openGameDialog(
    dialogData: GameDialogData,
    config?: DynamicDialogConfig,
    afterClosed?: () => any,
  ) {
    if (!dialogData) {
      return false;
    }
    const dialogConfig: DynamicDialogConfig = {
      ...config,
      data: dialogData,
    };
    const ref = this.open(GameDialogComponent, dialogConfig, afterClosed);
    return ref;
  }

  private open<T, D = any>(
    componentOrTemplateRef: ComponentType<T>,
    config?: DynamicDialogConfig<D>,
    afterClosed?: () => any,
  ): DynamicDialogRef {
    const ref = this.dynamicDialogService.open(componentOrTemplateRef, config);
    ref.afterClosed$.subscribe(() => {
      if (!afterClosed) { return; }
      afterClosed();
    });
    return ref;
  }
}

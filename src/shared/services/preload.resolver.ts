import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { PreloadService } from './preload.service';

@Injectable()
export class PreloadResolver implements Resolve<any> {
  constructor(
    private readonly preloadService: PreloadService,
  ) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const scenePrefix = route.data?.scenePrefix;
    const promises = this.preloadService.preloadPromises(scenePrefix);
    await Promise.all(promises);
  }
}

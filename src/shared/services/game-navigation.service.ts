import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { GameBoardService } from './game-board.service';
import { GameComponentService } from './game-component.service';

enum ScrollBehaviorValue {
  AUTO = 'auto',
  SMOOTH = 'smooth',
}

@Injectable()
export class GameNavigationService extends GameComponentService {
  private _scrollPercent = 50;
  private _scrollLeft = 0;
  private _scrollTop = 0;

  public get scrollLeft() {
    return this._scrollLeft;
  }

  public get scrollTop() {
    return this._scrollTop;
  }

  constructor(private readonly gameBoardService: GameBoardService) {
    super();
  }

  public init() {
    this.subscribeScrollEvent();
  }

  public scroll(options: ScrollToOptions) {
    const element = this.getScrollElement();
    element.scroll(options);
  }

  public scrollToStartX(behavior: ScrollBehavior = ScrollBehaviorValue.SMOOTH) {
    const element = this.getScrollElement();
    element.scroll({ behavior, left: 0 });
  }

  public scrollToEndX(behavior: ScrollBehavior = ScrollBehaviorValue.SMOOTH) {
    const element = this.getScrollElement();
    const maxWidth = element.scrollWidth;
    const viewWidth = element.clientWidth;
    const offsetX = maxWidth - viewWidth;
    element.scroll({ behavior, left: offsetX });
  }

  public scrollToCenterX(behavior: ScrollBehavior = ScrollBehaviorValue.AUTO) {
    const element = this.getScrollElement();
    const maxWidth = element.scrollWidth;
    const viewWidth = element.clientWidth;
    const offsetX = (maxWidth - viewWidth) / 2;
    element.scroll({ behavior, left: offsetX });
  }

  public scrollLeftByPercent(percent: number = 0, behavior: ScrollBehavior = ScrollBehaviorValue.SMOOTH) {
    percent = Math.max(0, Math.min(percent || this._scrollPercent, 100));
    const element = this.getScrollElement();
    const maxWidth = element.scrollWidth;
    const offsetLeft = element.scrollLeft;
    const offsetX = Math.max(offsetLeft - maxWidth * (percent / 100), 0);
    element.scroll({ behavior, left: offsetX });
  }

  public scrollRightByPercent(percent: number = 0, behavior: ScrollBehavior = ScrollBehaviorValue.SMOOTH) {
    percent = Math.max(0, Math.min(percent || this._scrollPercent, 100));
    const element = this.getScrollElement();
    const maxWidth = element.scrollWidth;
    const offsetLeft = element.scrollLeft;
    const offsetX = offsetLeft + maxWidth * (percent / 100);
    element.scroll({ behavior, left: offsetX });
  }

  private getScrollElement(): HTMLElement {
    const element = this.gameBoardService.gameDragScrollContent;
    if (!element.scroll) {
      // @ts-ignore
      element.scroll = (options: ScrollToOptions) => {
        element.scrollLeft = options?.left;
      };
    }
    return element;
  }

  private subscribeScrollEvent() {
    const target = this.getScrollElement();
    this.subscribe(
      fromEvent(target, 'scroll'),
      (ev: Event) => {
        this._scrollLeft = target?.scrollLeft || 0;
        this._scrollTop = target?.scrollTop || 0;
      },
    );
  }
}

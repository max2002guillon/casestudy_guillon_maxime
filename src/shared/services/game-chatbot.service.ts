import { Injectable } from '@angular/core';
import { EMPTY, Observable, Subject } from 'rxjs';
import { GameComponentService } from './game-component.service';

@Injectable()
export class GameChatBotService extends GameComponentService {
  private dialogContentObservable: Observable<string> = EMPTY;

  private dialogContentSubject = new Subject<Observable<string>>();

  public btnActionText$: Observable<string>;
  public btnActionEvent: () => void;
  public actionClose: () => void;

  public getDialogContentObservable() {
    return this.dialogContentSubject.asObservable();
  }

  constructor() {
    super();
  }

  public setContent(contentObservable: Observable<string>) {
    this.dialogContentObservable = contentObservable;
    this.dialogContentSubject.next(this.dialogContentObservable);
  }

  public setBtnActionConfig(btnText$: Observable<string>, action: () => any) {
    this.btnActionText$ = btnText$;
    this.btnActionEvent = action;
  }

  public clearAction() {
    this.btnActionText$ = null;
  }
}

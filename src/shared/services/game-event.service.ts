import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { GameEvent } from '../interfaces/game-event.interface';

@Injectable({ providedIn: 'root' })
export class GameEventService {
  private eventSubject = new Subject<GameEvent>();

  public get eventObservable(): Observable<GameEvent> {
    return this.eventSubject.asObservable();
  }

  public observe(name: string | number) {
    return this.eventObservable.pipe(
      filter(v => ![undefined, null].includes(v.name) && v.name === name),
      tap(v => {
      }),
    );
  }

  public emit(gameEvent: GameEvent) {
    this.eventSubject.next(gameEvent);
  }
}

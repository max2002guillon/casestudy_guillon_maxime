import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { assets } from '../../environments/assets';
import { environment } from '../../environments/environment';
import { AssetService } from '../assets/services/asset.service';
import { Subscribable } from '../components/base-component/subscribable';
import { GameConstants } from '../constants/game.constant';
import { ResponsiveLayoutService } from './responsive-layout.service';

export function preloadAppInitializer(preloadService: PreloadService): any {
  return () => preloadService.preloadAppInitializer();
}

@Injectable({ providedIn: 'root' })
export class PreloadService extends Subscribable {
  private _preloadedSubject = new BehaviorSubject<boolean>(false);
  private imagesLoaded = [];

  private isMobile = false;

  constructor(
    private readonly assetService: AssetService,
    private readonly responsiveLayoutService: ResponsiveLayoutService
  ) {
    super();
    this.subscribe(
      this.responsiveLayoutService.isMobileLayout(),
      (result) => this.isMobile = result
    );
  }

  preloadObs() {
    return this._preloadedSubject.asObservable();
  }

  public preload() {
    const images = this.getImageUrls();
    this.preloadImages(images);
  }

  public preloadAppInitializer(): Promise<any> {
    return new Promise((resolve: any) => {
      const images = this.getImageUrls();
      this.preloadImages(images, resolve);
    });
  }

  public preloadPromises(scenePrefix: string) {
    const images = this.getImageUrls(scenePrefix);
    return this.preloadImagesPromises(images);
  }

  private getImageUrls(scenePrefix?: string) {
    const images = [];
    const filteredKeys = this.getAssetKeys(scenePrefix);
    filteredKeys.forEach(key => {
      const value = this.assetService.getUrl(key) || this.assetService.getI18nUrl(key);
      if (!value) {
        return;
      }
      if (Array.isArray(value)) {
        images.push(...value);
      } else {
        images.push(value);
      }
    });
    return images;
  }

  private getAssetKeys(scenePrefix?: string): string[] {
    let filteredKeys = [];
    const assetKeys = Object.keys(assets);
    const keys = [];

    assetKeys.forEach(key => {
      // General key
      if (typeof assets[key] === 'string' || assets[key] instanceof String) {
        keys.push(key);
      } else {
        // Keys by language
        keys.push(...Object.keys(assets[key]));
      }
    });

    const uniqueKeys = [...new Set(keys)];

    if (scenePrefix) {
      filteredKeys = uniqueKeys.filter(k => k.indexOf(scenePrefix) !== -1);
    } else {
      filteredKeys = uniqueKeys;
    }

    if (this.isMobile) {
      filteredKeys = filteredKeys.filter(k => k.indexOf(GameConstants.PRELOAD_MOBILE_PREFIX) !== -1);
    } else {
      filteredKeys = filteredKeys.filter(k => k.indexOf(GameConstants.PRELOAD_MOBILE_PREFIX) === -1);
    }
    return filteredKeys;
  }

  private preloadImages(images: string[], resolve?: any) {
    const imageLength = images.length;
    let totalLoaded = 0;
    images.forEach((x, i) => {
      if (this.imagesLoaded.findIndex((y) => x === y) > -1) {
        return;
      }
      this.imagesLoaded[i] = this.newImage(x, () => {
        totalLoaded++;
        if (!environment.production) {
          console.info(`loading: ${Math.floor((totalLoaded * 100) / imageLength)}`);
        }
        if (totalLoaded === imageLength) {
          if (!environment.production) {
            console.info('loaded all assets');
          }
          this._preloadedSubject.next(true);
          if (resolve) {
            resolve();
          }
        }
      });
    });
  }

  private preloadImagesPromises(images: string[]) {
    const promises = [];
    const imageLength = images.length;
    let totalLoaded = 0;
    images.forEach((x, i) => {
      if (this.imagesLoaded.findIndex((y) => x === y) > -1) {
        return;
      }
      promises.push(new Promise((resolve: any) => {
        this.imagesLoaded[i] = this.newImage(x, () => {
          totalLoaded++;
          if (!environment.production) {
            console.info(`loading: ${Math.floor((totalLoaded * 100) / imageLength)}`);
          }
          resolve();
        }, () => {
          resolve();
        });
      }));
    });
    return promises;
  }

  private newImage(src: string, onload?, onerror?) {
    const image = new Image();
    image.src = src;
    image.onload = onload;
    image.onerror = onerror;
    return image;
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GameComponentService } from './game-component.service';

@Injectable()
export class GameProductivityService extends GameComponentService {
  private productivityLadderSubject = new BehaviorSubject<number>(0);

  public get productivityLadder$() {
    return this.productivityLadderSubject.asObservable();
  }

  public setProductivityLadder(numbOfLadder: number) {
    this.productivityLadderSubject.next(numbOfLadder);
  }
}

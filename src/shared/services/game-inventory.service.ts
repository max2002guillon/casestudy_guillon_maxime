import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GameInventoryAction } from '../components/game-inventory/enums/game-inventory-action.enum';
import { GameInventoryItem } from '../interfaces/game-inventory-item.interface';
import { GameUtil } from './../utils/game.util';
import { GameComponentService } from './game-component.service';

interface AddItemOptions {
  skipDupeCheck?: boolean;
}

@Injectable()
export class GameInventoryService extends GameComponentService {
  public label = 'OUVRIR';
  public items: GameInventoryItem[] = [];
  public listConnectId: string[] = [];

  private actionSubject = new Subject();

  public get actionObservable(): Observable<any> {
    return this.actionSubject.asObservable();
  }

  public open(): void {
    this.actionSubject.next(GameInventoryAction.OPEN);
  }

  public close(): void {
    this.actionSubject.next(GameInventoryAction.CLOSE);
  }

  public hasItem(id: string): boolean {
    const index = this.items.findIndex(v => v.id === id);
    const hasItem = index >= 0;
    return hasItem;
  }

  public addItem(item: GameInventoryItem, options?: AddItemOptions): void {
    if (!this.items) {
      this.items = [];
    }
    if (!options?.skipDupeCheck && this.hasItem(item.id)) {
      return;
    }
    this.items.push(item);

    // Remove item from the background
    if (item.hiddenFromBackground) {
      GameUtil.hideElementById(item.id);
    }
  }

  /**
   * Only the games have available inventory items
   */
  public replaceItem(item: GameInventoryItem) {
    if (!this.items) {
      this.items = [];
    }
    this.items[this.items.findIndex(v => v.id === item.id)] = item;

    // Remove item from the background
    if (item.hiddenFromBackground) {
      GameUtil.hideElementById(item.id);
    }
  }

  public removeItemById(id: string): void {
    if (!this.items) {
      this.items = [];
    }

    this.items = this.items.filter(x => x.id !== id);
  }

  public activeItem(id: string, url: string): void {
    const item = this.items.find(v => v.id === id);
    if (!item) {
      return;
    }
    item.isActive = true;
    item.imgSrcActive = url;
  }

  public clearItems(): void {
    this.items = [];
  }

  public scrollToPreviousItem(scrollElement: HTMLElement): void {
    const childTotalWidth = this.getLastItemTotalWidth(scrollElement);
    const offsetX = Math.max(scrollElement.scrollLeft - childTotalWidth, 0);
    scrollElement.scroll({ behavior: 'smooth', left: offsetX });
  }

  public scrollToNextItem(scrollElement: HTMLElement): void {
    const childTotalWidth = this.getLastItemTotalWidth(scrollElement);
    const offsetX = scrollElement.scrollLeft + childTotalWidth;
    scrollElement.scroll({ behavior: 'smooth', left: offsetX });
  }

  private getLastItemTotalWidth(scrollElement: HTMLElement): number {
    const children = Array.from(scrollElement.children);
    const child = children.pop();
    if (!child) {
      return null;
    }
    const childStyles = window.getComputedStyle(child);
    const width = child.scrollWidth;
    const withWithoutContent = [
      childStyles.marginLeft,
      childStyles.marginRight,
      childStyles.paddingLeft,
      childStyles.paddingRight,
    ].map(v => parseInt(v, 10)).reduce((pv, cv) => pv + cv, 0);
    const childTotalWidth = width + withWithoutContent;
    return childTotalWidth;
  }
}

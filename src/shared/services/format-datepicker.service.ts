import { NativeDateAdapter } from '@angular/material/core';
import { Injectable } from '@angular/core';

@Injectable()
export class FormatDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: string): string {
    if (displayFormat === 'input') {
      const day = this.getData(date.getDate().toString());
      const month = this.getData((date.getMonth() + 1).toString());
      const year = date.getFullYear().toString();
      return `${day}/${month}/${year}`;
    } else {
      return date.getFullYear().toString();
    }
  }
  getData(value) {
    value = Number.parseInt(value, 10);
    const prefix = value < 10 ? '0' : '';
    return (prefix + value).toString();
  }
}


export const FORMAT_DD_MM_YYYY = {
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'short', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' }
  }
};
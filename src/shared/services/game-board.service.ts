import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Subject } from 'rxjs';
import { GameLoadingDialogComponent } from '../components/game-loading-dialog/game-loading-dialog.component';
import { GameHtmlService } from './game-html.service';
import { GameHttpService } from './game-http.service';

@Injectable({ providedIn: 'root' })
export class GameBoardService {
  public boardBackgroundUrl: string;
  public isDisabledScroll = false;

  private readonly DRAG_SCROLL_ID = 'drag-scroll';
  private readonly BOARD_FOREGROUND_ID = 'board-foreground';

  private boardScrollDisabledSubject = new BehaviorSubject<boolean>(false);
  private boardBackgroundImageUrlSubject = new BehaviorSubject<string>(null);
  private boardLoadedSubject = new Subject();

  constructor(
    private readonly gameHttpService: GameHttpService,
    private readonly gameHtmlService: GameHtmlService,
    private readonly dialog: MatDialog
  ) {}

  public get gameDragScrollContent(): HTMLElement {
    return document.getElementById(this.DRAG_SCROLL_ID);
  }

  public get gameBoardForeground(): HTMLElement {
    return document.getElementById(this.BOARD_FOREGROUND_ID);
  }

  public getBoardScrollDisabledObservable() {
    return this.boardScrollDisabledSubject.asObservable();
  }

  public getBoardBackgroundImageUrlObservable() {
    return this.boardBackgroundImageUrlSubject.asObservable();
  }

  public getBoardLoadedObservable() {
    return this.boardLoadedSubject.asObservable();
  }

  public async loadScene(svgUrl?: string, imgUrl?: string, showLoading = false) {
    let loadingRef;
    if (showLoading) {
      loadingRef = this.dialog.open(GameLoadingDialogComponent, {
        disableClose: true,
        panelClass: 'game-loading',
      });
    }

    await this.loadBoardBackgroundImage(imgUrl);
    await this.loadBoardForegroundSvg(svgUrl);

    this.boardLoadedSubject.next();

    // Close loading
    if (loadingRef) {
      loadingRef.close();
    }
  }

  private loadBoardBackgroundImage(url?: string) {
    if (!url) {
      return new Promise<void>((resolve, reject) => resolve());
    }

    return new Promise<void>((resolve, reject) => {
      const img = new Image();
      img.addEventListener('load', () => {
        this.boardBackgroundImageUrlSubject.next(url);
        resolve();
      });
      img.addEventListener('error', () => {
        reject(new Error(`Failed to load image URL: ${url}`));
      });
      img.src = url;
    });
  }

  public async loadBoardForegroundSvg(svgUrl?: string) {
    if (!svgUrl) {
      return;
    }
    this.gameBoardForeground.innerHTML = '';
    const svgData = await this.gameHttpService.getSvgData(svgUrl);
    const svgElement = this.gameHtmlService.createSvgElement(svgData);
    this.gameBoardForeground.appendChild(svgElement);
  }

  public async loadSvgByElementId(id: string, backgroundUrl: string, width = '100%', height = '100%') {
    const data = await this.gameHttpService.getSvgData(backgroundUrl);
    const svgElement = this.gameHtmlService.createSvgElement(data);
    svgElement.style.width = width;
    svgElement.style.height = height;
    const element = document.getElementById(id);
    element.innerHTML = '';
    element.appendChild(svgElement);
  }

  public fixSvgPatternFillUrl(id = 'board-foreground') {
    Array.from(document.querySelectorAll(`#${id} [id][fill^=url]`) || [])
      .forEach(element => {
        let fill = element.getAttribute('fill');
        fill = fill.replace('url(#', 'url(' + window.location.origin + window.location.pathname + '#');
        element.setAttribute('fill', fill);
      });
  }
}

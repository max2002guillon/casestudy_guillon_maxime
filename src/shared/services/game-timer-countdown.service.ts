import { Injectable, OnDestroy } from '@angular/core';
import { Subject, Subscription, timer } from 'rxjs';
import { AppRouterService } from '../../app/services/app-router.service';
import { GameComponentService } from './game-component.service';

@Injectable()
export class GameTimerCountDownService extends GameComponentService implements OnDestroy {
  private countDownTime = 0;

  private timerTickSubject = new Subject<number>();
  private timerSubscription: Subscription;

  private _cachePastTime = 0;

  private readonly TIME_OVER = 45 * 60 * 1000;
  private readonly GAME_COMPLETED_ROUTER_COMMAND = ['game-completed'];

  constructor(
    private readonly router: AppRouterService,
  ) {
    super();
    this.countDownTime = this.TIME_OVER;
  }

  public getTimerTickObservable() {
    return this.timerTickSubject.asObservable();
  }

  public start() {
    this.stop();
    this.initCountDownTime();
  }

  public stop() {
    this.setTimerSubscription();
  }

  public getTheRestTimer() {
    return this.countDownTime;
  }

  public getThePastTimer() {
    return this.TIME_OVER - this.countDownTime;
  }

  public setTheRestTime(value: number) {
    this.countDownTime = this.TIME_OVER - value;
    this._cachePastTime = value;
  }

  public setCachePastTime(value: number) {
    this._cachePastTime = value;
  }

  public getCachePastTime() {
    return this._cachePastTime;
  }

  ngOnDestroy() {
    this.stop();
    super.ngOnDestroy();
  }

  private initCountDownTime(): void {
    this.setTimerSubscription(timer(0, 1000).subscribe(() => {
      this.countDownTime = this.countDownTime - 1000;
      this.timerTickSubject.next(this.countDownTime);

      if (this.countDownTime < 0) {
        this.setTimerSubscription();
        this.router.navigate(this.GAME_COMPLETED_ROUTER_COMMAND,  { queryParams: { isOver: true }});
      }
    }));
  }

  private setTimerSubscription(value?: Subscription) {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    this.timerSubscription = value;
  }
}

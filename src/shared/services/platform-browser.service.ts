import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class PlatformBrowserService {
  constructor(
    @Inject(PLATFORM_ID) private readonly platformId,
  ) {}

  public get isPlatformBrowser() {
    return isPlatformBrowser(this.platformId);
  }
}
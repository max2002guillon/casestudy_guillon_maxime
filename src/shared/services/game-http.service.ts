import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GameHttpService {
  constructor(private readonly http: HttpClient) { }

  public async getSvgData(url: string): Promise<string> {
    const headers = this.getSvgHeaders();
    const data = await this.http.get(url, { headers, responseType: 'text' }).toPromise();
    return data;
  }

  private getSvgHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    headers.set('Accept', 'image/svg+xml');
    return headers;
  }
}

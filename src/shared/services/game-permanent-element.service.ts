import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PermanentElementPosition } from '../interfaces/game-permanent-element.interface';
import { GameUtil } from '../utils/game.util';

@Injectable({ providedIn: 'root' })
export class GamePermanentElementService {
  protected positionSubject = new BehaviorSubject<PermanentElementPosition[]>(null);

  public getElementPosition$(): Observable<PermanentElementPosition[]> {
    return this.positionSubject.asObservable();
  }

  public listenElementPositionChange(elementId: string): void {
    const positions: PermanentElementPosition[] = [];
    const elements = document.querySelectorAll(`[id^=${elementId}`);
    if (elements.length > 0) {
      for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        const rect = element.getBoundingClientRect();
        const browserSize = GameUtil.getBrowserSize();
        const elementPositionLeft = (rect.left / browserSize.width) * 100;
        const elementPositionRight = (browserSize.width - rect.right) / browserSize.width * 100;
        const elementPositionTop = (rect.top / browserSize.height) * 100;
        const elementPositionBottom = (browserSize.height - rect.bottom) / browserSize.height * 100;

        const position: PermanentElementPosition = {
          elementId: element.id,
          left: +elementPositionLeft.toFixed(2),
          right: +elementPositionRight.toFixed(2),
          top: +elementPositionTop.toFixed(2),
          bottom: +elementPositionBottom.toFixed(2),
        };
        positions.push(position);
      }
      this.positionSubject.next(positions);
    }
  }
}

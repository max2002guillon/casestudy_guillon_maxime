import { Directive } from '@angular/core';
import { Subscribable } from '../components/base-component/subscribable';
import { GameFrameItem } from '../interfaces/game-frame-item.interface';

@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class BaseGameControlService extends Subscribable {
  public readonly ITEM_CLASS_CLICKABLE = 'clickable';

  public frameItems: GameFrameItem[];

  protected getIdFromMouseEvent(ev: MouseEvent): string {
    const target = ev.currentTarget as HTMLElement;
    const id = target.id;
    return id;
  }

  protected initFrameItem(item: GameFrameItem) {
    const element = document.getElementById(item.id);
    this.addEventForElement(item, element);
  }

  protected initFrameItems() {
    this.frameItems.forEach(v => this.initFrameItem(v));
  }

  protected removeFrameItem(id: string) {
    if (!this.frameItems) {
      return;
    }
    const index = this.frameItems.findIndex(v => v.id === id);
    if (index >= 0) {
      this.frameItems.splice(index, 1);
    }
    const el = document.getElementById(id);
    if (!el) {
      return;
    }
    this.removeClickable(el);
  }

  protected removeClickable(element: HTMLElement): void {
    if (!element) {
      return;
    }
    element.classList.remove(this.ITEM_CLASS_CLICKABLE);
    element.onmousedown = null;
    element.onmouseup = null;
    element.onmouseenter = null;
    element.onmouseleave = null;
  }

  protected clearFrameItems() {
    if (!this.frameItems) {
      return;
    }
    this.frameItems.forEach(x => {
      const el = document.getElementById(x.id);
      if (el) {
        this.removeClickable(el);
      }
    });
  }

  protected addFrameItem(item: GameFrameItem) {
    this.initFrameItem(item);
    if (!this.frameItems) {
      this.frameItems = [];
    }
    this.frameItems.push(item);
  }

  private addEventForElement(item: GameFrameItem, element: HTMLElement): void {
    if (!element) {
      return;
    }
    if (item.classes) {
      element.classList.add(...item.classes);
    }
    if (item.mousedown) {
      element.classList.add(this.ITEM_CLASS_CLICKABLE);
      element.onmousedown = item.mousedown;
    }
    if (item.mouseup) {
      element.onmouseup = item.mouseup;
    }
    if (item.mouseenter) {
      element.onmouseenter = item.mouseenter;
    }
    if (item.mouseleave) {
      element.onmouseleave = item.mouseleave;
    }
    if (item.focusout) {
      element.onblur = item.focusout;
    }
  }
}

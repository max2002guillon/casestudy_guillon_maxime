import { Injectable, Optional } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable()
export class NgSwLogUpdateService {
  constructor(@Optional() private readonly updates: SwUpdate) {}

  init() {
    if (this.updates) {
      this.updates.available.subscribe((event) => {
        console.info('current version is', event.current);
        console.info('available version is', event.available);
      });
      this.updates.activated.subscribe((event) => {
        console.info('old version was', event.previous);
        console.info('new version is', event.current);
      });
    }
  }
}

import { Injectable, OnDestroy } from '@angular/core';

@Injectable()
export abstract class GameAudio implements OnDestroy {
  protected audio: HTMLAudioElement;

  public set src(value: string) {
    this.audio.src = value;
  }

  public get volume() {
    return this.audio.volume;
  }

  public set volume(value: number) {
    this.audio.volume = value;
  }

  public get muted() {
    return this.audio.muted;
  }

  public set muted(value: boolean) {
    this.audio.muted = value;
  }

  public get currentTime() {
    return this.audio.currentTime;
  }

  public set currentTime(value: number) {
    this.audio.currentTime = value;
  }

  public get loop() {
    return this.audio.loop;
  }

  public set loop(value: boolean) {
    this.audio.loop = value;
  }

  constructor() {
    const audio = document.createElement('audio');
    this.audio = audio;
  }

  ngOnDestroy() {
    this.audio.remove();
  }

  public play(src?: string) {
    if (src) {
      this.audio.src = src;
    }
    if (!this.audio.src) {
      return null;
    }
    if (!this.audio.volume) {
      return null;
    }
    return this.audio
      .play()
      .catch(() => {
        // debugger
      });
  }

  public pause() {
    return this.audio.pause();
  }

  public setSrc(value: string) {
    this.src = value;
  }
}

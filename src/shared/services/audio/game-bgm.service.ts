import { Injectable } from '@angular/core';
import { GameAudio } from './game-audio.class';

@Injectable()
export class GameBgmService extends GameAudio {
  constructor() {
    super();
    this.audio.loop = true;
  }
}

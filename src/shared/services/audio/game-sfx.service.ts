import { Injectable } from '@angular/core';
import { GameAudio } from './game-audio.class';

@Injectable()
export class GameSfxService extends GameAudio {
  public play(src?: string) {
    this.pause();
    this.currentTime = 0;
    return super.play(src);
  }
}

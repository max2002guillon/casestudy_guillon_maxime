import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { GameComponentService } from '../game-component.service';
import { GameBgmService } from './game-bgm.service';
import { GameSfxService } from './game-sfx.service';

@Injectable()
export class GameSoundService extends GameComponentService {
  private isMuted = true;
  private muteIcon: string;
  private unmuteIcon: string;

  private audioMutedSubject = new BehaviorSubject<boolean>(this.isMuted);
  private soundMove = new BehaviorSubject<boolean>(false);

  public get iconSrc(): string {
    const src = this.isMuted
      ? this.muteIcon
      : this.unmuteIcon;
    return src;
  }

  constructor(
    private readonly gameBgmService: GameBgmService,
    private readonly gameSfxService: GameSfxService,
  ) {
    super();
    this.gameBgmService.muted = this.isMuted;
    this.gameSfxService.muted = this.isMuted;
  }

  public toggleMute(muted?: boolean) {
    this.isMuted = muted !== undefined
      ? !!muted
      : !this.isMuted;

    this.gameBgmService.muted = this.isMuted;
    this.gameSfxService.muted = this.isMuted;
    if (this.isMuted) {
      this.gameBgmService.pause();
      this.gameSfxService.pause();
    } else {
      this.gameBgmService.play();
    }

    this.audioMutedSubject.next(this.isMuted);
  }

  public playBgm() {
    if (this.isMuted) {
      return null;
    }
    return this.gameBgmService.play();
  }

  public getAudioMutedObservable(): Observable<boolean> {
    return this.audioMutedSubject.asObservable();
  }

  public setMuteIcon(src: string) {
    this.muteIcon = src;
  }

  public setUnmuteIcon(src: string) {
    this.unmuteIcon = src;
  }

  public getSoundMoveObservable(): Observable<boolean> {
    return this.soundMove.asObservable();
  }

  public setSoundMoveObservable(isMove: boolean) {
    this.soundMove.next(isMove);
  }
}

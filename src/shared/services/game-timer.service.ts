import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription, timer } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { GameComponentService } from './game-component.service';

@Injectable({ providedIn: 'root' })
export class GameTimerService extends GameComponentService implements OnDestroy {
  private readonly TIMER_TICK_INTERVAL = 1000;
  private readonly TIMER_PENALTY_VISIBLE_TIME = 2500;

  private timerTickSubject = new Subject<number>();
  private timerObservable: Observable<number>;
  private timerSubscription: Subscription;
  private timerPenaltyVisibleSubject = new BehaviorSubject<boolean>(false);
  private timerPenaltyHideObservable: Observable<any>;
  private timerPenaltyHideSubscription: Subscription;

  private startTime: number;
  private pastElapsedTime = 0;
  private curCacheElapsedTime = 0;
  private curElapsedTime = 0;
  private curPenaltyTime = 0;

  constructor() {
    super();
    this.timerObservable = timer(0, this.TIMER_TICK_INTERVAL).pipe(
      map(() => this.calcElapsedTime()),
      tap(elapsedTime => this.timerTickSubject.next(elapsedTime)),
    );
    this.timerPenaltyHideObservable = timer(this.TIMER_PENALTY_VISIBLE_TIME).pipe(
      tap(() => this.timerPenaltyVisibleSubject.next(false)),
    );
  }

  public start() {
    this.stop();
    this.pastElapsedTime += this.curCacheElapsedTime + this.curElapsedTime + this.curPenaltyTime;
    this.curCacheElapsedTime = 0;
    this.curElapsedTime = 0;
    this.curPenaltyTime = 0;
    this.startTime = Date.now();
    this.subscribeTimer();
  }

  public stop() {
    if (!this.timerSubscription) {
      return;
    }
    this.timerSubscription.unsubscribe();
    this.timerSubscription = null;
    this.emitTimeTick();
  }

  public unpause() {
    this.pause();
    this.curCacheElapsedTime += this.curElapsedTime + this.curPenaltyTime;
    this.curElapsedTime = 0;
    this.curPenaltyTime = 0;
    this.startTime = Date.now();
    this.subscribeTimer();
  }

  public pause() {
    this.stop();
  }

  public getTimerTickObservable() {
    return this.timerTickSubject.asObservable();
  }

  public getTimerPenaltyVisibleObservable() {
    return this.timerPenaltyVisibleSubject.asObservable();
  }

  public getCurrentElapsedTime(): number {
    const value = this.curCacheElapsedTime + this.curElapsedTime + this.curPenaltyTime;
    return value;
  }

  public getTotalElapsedTime(): number {
    const value = this.pastElapsedTime + this.getCurrentElapsedTime();
    return value;
  }

  public setPastElapsedTime(value: number) {
    this.pastElapsedTime = value;
  }

  public getTimerString(milliseconds: number, separator = ':'): string {
    if (isNaN(milliseconds) || milliseconds === null) {
      milliseconds = 0;
    }
    const ss = Math.floor(milliseconds / 1000) % 60;
    const mm = Math.floor(milliseconds / 1000 / 60) % 60;
    const hh = Math.floor(milliseconds / 1000 / 60 / 60);
    const value = [hh, mm, ss].map(v => String(v).padStart(2, '0')).join(separator);
    return value;
  }

  public addPenaltyTime(milliseconds: number = 30000) {
    if (this.timerPenaltyHideSubscription) {
      this.timerPenaltyHideSubscription.unsubscribe();
    }
    this.curPenaltyTime += milliseconds || 0;
    this.emitTimeTick();
    this.timerPenaltyVisibleSubject.next(true);
    this.timerPenaltyHideSubscription = this.timerPenaltyHideObservable.subscribe();
  }

  ngOnDestroy() {
    this.stop();
    super.ngOnDestroy();
  }

  private calcElapsedTime(): number {
    if ([undefined, null].includes(this.startTime)) {
      return 0;
    }
    const curTime = Date.now();
    const deltaTime = curTime - this.startTime;
    this.curElapsedTime = deltaTime;
    return this.getTotalElapsedTime();
  }

  private subscribeTimer() {
    if (!this.timerObservable) {
      return;
    }
    this.timerSubscription = this.timerObservable.subscribe();
  }

  private emitTimeTick() {
    this.timerTickSubject.next(this.calcElapsedTime());
  }
}

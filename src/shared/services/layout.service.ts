import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Subscribable } from '../components/base-component/subscribable';

export enum Layout {
  MOBILE,
  MOBILE_LANDSCAPE,
  TABLET,
  DESKTOP,
}

@Injectable({ providedIn: 'root' })
export class LayoutService extends Subscribable {
  private readonly LAYOUT_BREAKPOINTS_LIST = [
    { layout: Layout.MOBILE, breakpoints: Breakpoints.HandsetPortrait },
    { layout: Layout.MOBILE_LANDSCAPE, breakpoints: Breakpoints.HandsetLandscape },
    { layout: Layout.TABLET, breakpoints: Breakpoints.Tablet },
    { layout: Layout.DESKTOP, breakpoints: Breakpoints.Web },
  ];

  private currentLayoutSubject: BehaviorSubject<Layout>;

  constructor(
    private readonly breakpointObserver: BreakpointObserver,
  ) {
    super();

    this.currentLayoutSubject = new BehaviorSubject(Layout.DESKTOP);
    this.initSubscriptions();
  }

  public getLayoutObservable() {
    return this.currentLayoutSubject
      .asObservable()
      .pipe(distinctUntilChanged());
  }

  public isMobile() {
    return this.currentLayoutSubject
      .asObservable()
      .pipe(
        map(v => v === Layout.MOBILE || v === Layout.MOBILE_LANDSCAPE),
        distinctUntilChanged(),
      );
  }

  public isTablet() {
    return this.currentLayoutSubject
      .asObservable()
      .pipe(
        map(v => v === Layout.TABLET),
        distinctUntilChanged(),
      );
  }

  public isDesktop() {
    return this.currentLayoutSubject
      .asObservable()
      .pipe(
        map(v => v === Layout.DESKTOP),
        distinctUntilChanged(),
      );
  }

  private initSubscriptions() {
    this.LAYOUT_BREAKPOINTS_LIST.forEach(v => {
      const subscription = this.breakpointObserver
        .observe(v.breakpoints)
        .pipe(filter(view => view.matches))
        .subscribe(() => {
          this.currentLayoutSubject.next(v.layout);
        });
      this.addSubscription(subscription);
    });
  }
}

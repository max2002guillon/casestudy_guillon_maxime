import { Injectable, Optional } from '@angular/core';
import { SwUpdate, UpdateAvailableEvent } from '@angular/service-worker';
import { Subscribable } from '../../components/base-component/subscribable';

@Injectable()
export class NgSwPromptUpdateService extends Subscribable {
  constructor(@Optional() private readonly updates: SwUpdate) {
    super();
  }

  init() {
    if (this.updates) {
      this.subscribe(this.updates.available, (event) => {
        if (this.promptUser(event)) {
          this.updates.activateUpdate().then(() => document.location.reload());
        }
      });
    }
  }

  promptUser(event: UpdateAvailableEvent) {
    // TODO: Notify or ask user if she wanna reload page.
    return true;
  }
}

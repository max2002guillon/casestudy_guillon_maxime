import { Injectable, Optional } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Subscribable } from '../../components/base-component/subscribable';

@Injectable()
export class NgSwLogUpdateService extends Subscribable {
  constructor(@Optional() private readonly updates: SwUpdate) {
    super();
  }

  init() {
    if (this.updates) {
      this.subscribe(this.updates.available, (event) => {
        console.info('current version is', event.current);
        console.info('available version is', event.available);
      });
      this.subscribe(this.updates.activated, (event) => {
        console.info('old version was', event.previous);
        console.info('new version is', event.current);
      });
    }
  }
}

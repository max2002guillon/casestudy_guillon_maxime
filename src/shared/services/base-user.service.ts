import { Inject, Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import { BehaviorSubject } from 'rxjs';
import { Subscribable } from '../components/base-component/subscribable';
import { CommonConstant } from '../constants/common.constant';
import { STORAGE } from '../storages/storages.inject';

@Injectable({ providedIn: 'root' })
export class BaseUserService extends Subscribable {
  protected userProfile: any;

  private token: string;
  private refreshToken: string;
  private user: any;

  private logged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  private _userProfileLoaded: BehaviorSubject<boolean> = new BehaviorSubject<any>(false);

  constructor(
    @Inject(STORAGE)
    protected readonly storage: Storage
  ) {
    super();
    this.refreshUserToken();
  }

  public getUserProfile() {
    return this.userProfile;
  }

  public setUserProfile(userProfile) {
    this.userProfile = userProfile;
    this._userProfileLoaded.next(this.userProfile);
  }

  public setUserToken(signin: { token: string; refreshToken: string }) {
    this.storage.setItem(CommonConstant.USER_TOKEN, JSON.stringify(signin));
    this.setUser(signin);
  }

  public getToken() {
    return this.token;
  }

  public getRefreshToken() {
    return this.refreshToken;
  }
  public logout() {
    this.user = undefined;
    this.clearUserToken();
    this.setLogged(false);
  }

  public getUser() {
    return this.user;
  }

  public setLogged(status: boolean) {
    this.logged$.next(status);
  }

  public getLogged() {
    return this.logged$.asObservable();
  }

  public getUserProfileLoadedObs() {
    return this._userProfileLoaded.asObservable();
  }

  private refreshUserToken() {
    const userToken = this.storage.getItem(CommonConstant.USER_TOKEN);
    if (userToken) {
      const token = JSON.parse(userToken);
      this.setUser(token);
      this.setLogged(true);
    }
  }

  private setUser(signin: { token: string; refreshToken: string }) {
    this.token = signin.token;
    this.refreshToken = signin.refreshToken;
    this.user = jwt_decode(this.token);
    this.setLogged(true);
  }

  private clearUserToken() {
    this.storage.removeItem(CommonConstant.USER_TOKEN);
    this.token = undefined;
    this.refreshToken = undefined;
  }

}

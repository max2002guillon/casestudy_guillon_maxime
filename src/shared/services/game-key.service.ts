import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GameComponentService } from './game-component.service';

export enum ListKey {
  ORANGE = 'ORANGE',
  GREEN = 'GREEN',
  BLUE = 'BLUE',
  RED = 'RED',
  PURPLE = 'PURPLE',
  YELLOW = 'YELLOW',
  WHITE = 'WHITE',
  PINK = 'PINK'
}

@Injectable()
export class GameKeyService extends GameComponentService {
  private numbOfKeySubject = new BehaviorSubject(0);

  private listKey = [];

  constructor(
  ) {
    super();
  }

  public get numbOfKeyObservable() {
    return this.numbOfKeySubject.asObservable();
  }

  public increaseNumbOfKey(idKey?: ListKey) {
    if (this.listKey && this.listKey.length > 0) {
      const item = this.listKey.find(x => x.id === idKey);
      if (item.collected) {
        return;
      }
      item.collected = true;
    }

    this.numbOfKeySubject.next((this.numbOfKeySubject.value || 0) + 1);
  }

  public setNumbOfKey(numb: number) {
    this.numbOfKeySubject.next(numb);
  }

  public resetNumbOfKey() {
    this.numbOfKeySubject.next(0);
  }

  public init() {
    // TODO: map the key was collected with BE
    this.listKey = Object.keys(ListKey).map(x => {
      const item = {
        id: x,
        collected: false
      };
      return item;
    });
  }
}

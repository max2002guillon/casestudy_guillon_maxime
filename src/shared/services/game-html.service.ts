import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

@Injectable()
export class GameHtmlService {
  private renderer: Renderer2;

  constructor(private readonly rendererFactory2: RendererFactory2) {
    // Get an instance of Angular's Renderer2
    this.renderer = this.rendererFactory2.createRenderer(null, null);
  }

  public createSvgElement(data: string): SVGSVGElement {
    const parser = new DOMParser();
    const doc = parser.parseFromString(data, 'image/svg+xml');
    const element = doc.firstChild;
    return element as SVGSVGElement;
  }

  public createDivElement(): HTMLDivElement {
    // Use Angular's Renderer2 to create the div element
    return this.renderer.createElement('div');
  }

  public createLineElement(position) {
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line') as SVGLineElement;
    line.setAttribute('x1', position.x1);
    line.setAttribute('y1', position.y1);
    line.setAttribute('x2', position.x2);
    line.setAttribute('y2', position.y2);
    line.setAttribute('stroke', position.color);
    line.setAttribute('stroke-width', '14');
    line.setAttribute('is-drawing', 'true');
    return line;
  }
}

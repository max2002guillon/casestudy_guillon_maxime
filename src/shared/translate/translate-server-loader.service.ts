import {
  makeStateKey,
  StateKey,
  TransferState,
} from '@angular/platform-browser';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';

const FS: any = require('fs');

export class TranslateServerLoaderService implements TranslateLoader {
  public static readonly PREFIX = 'i18n';
  public static readonly SUFFIX = '.json';
  public static readonly FORMAT = 'utf8';

  constructor(
    private prefix: string = TranslateServerLoaderService.PREFIX,
    private suffix: string = TranslateServerLoaderService.SUFFIX,
    private transferState: TransferState
  ) {}

  public getTranslation(lang: string): Observable<any> {
    return new Observable((observer) => {
      const jsonData: any = JSON.parse(
        FS.readFileSync(
          `${this.prefix}/${lang}${this.suffix}`,
          TranslateServerLoaderService.FORMAT
        )
      );
      const key: StateKey<number> = makeStateKey<number>(
        `transfer-translate-${lang}`
      );
      this.transferState.set(key, jsonData);
      observer.next(jsonData);
      observer.complete();
    });
  }
}

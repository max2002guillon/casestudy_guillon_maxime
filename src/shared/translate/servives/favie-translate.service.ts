import { Inject, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscribable } from '../../components/base-component/subscribable';
import { PlatformBrowserService } from '../../services/platform-browser.service';
import { STORAGE } from '../../storages/storages.inject';
import * as languages from 'src/shared/jsons/locales.json';

export interface ILang {
  id: string;
  code: string;
  culture: string;
  name: string;
  localName: string;
  active: boolean;
  default: boolean;
}

@Injectable()
export class FavieTranslateService extends Subscribable {
  public static readonly LOCALE_STORAGE_KEY = 'LOCALE_STORAGE_KEY';

  constructor(
    @Inject(STORAGE)
    protected readonly storage: Storage,
    private readonly translateService: TranslateService,
    private readonly platformBrowserService: PlatformBrowserService,
  ) {
    super();
  }

  public switchLanguage(lang: string): void {
    this.translateService.setDefaultLang(lang);
  }

  public get langList(): ILang[] {
    return (languages  as  any).default;
  }

  public initAppLang() {
    let language = this.translateService.getBrowserLang();

    if (this.platformBrowserService.isPlatformBrowser) {
      language = language || window.navigator['userLanguage'] || window.navigator['language'];
    }

    const localStorage = this.storage.getItem(FavieTranslateService.LOCALE_STORAGE_KEY);
    if (localStorage) {
      language = localStorage;
    }

    const defaultLang = this.langList.find(x => x.active && x.default);
    const currentLang = this.langList.find(x => x.active && x.code === language) ? language : defaultLang.code;

    this.translateService.setDefaultLang(currentLang);
  }

  public setLanguage(lang: string) {
    const currentLang = this.langList.find(x => x.active && x.code === lang);
    if (currentLang) {
      this.translateService.setDefaultLang(currentLang.code);
    }
  }

  public getLanguage() {
    return this.translateService.getDefaultLang();
  }
}

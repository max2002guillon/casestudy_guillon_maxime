import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TransferState } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FavieTranslateService } from './servives/favie-translate.service';
import { TranslateLoaderService } from './translate-browser-loader.service';

export function createTranslateLoader(http: HttpClient, transferState: TransferState) {
  return new TranslateLoaderService('./assets/i18n/', '.json', http, transferState);
}

@NgModule({
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient, TransferState],
      },
    }),
  ],
  providers: [FavieTranslateService],
  exports: [TranslateModule],
})
export class TranslateBrowserModule {}

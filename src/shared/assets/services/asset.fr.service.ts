import { Injectable } from '@angular/core';
import { assets } from '../../../environments/assets';
import { BaseAssetService } from './base-asset.service';

@Injectable({ providedIn: 'root' })
export class AssetFrService extends BaseAssetService {
  public getUrl(key: string): string {
    const data = assets['fr'];
    const value = data[key];
    return value;
  }
}

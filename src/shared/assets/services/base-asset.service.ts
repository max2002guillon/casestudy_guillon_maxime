export abstract class BaseAssetService {
  public abstract getUrl(key: string): string;
}

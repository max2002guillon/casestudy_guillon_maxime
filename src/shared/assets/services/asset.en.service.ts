import { Injectable } from '@angular/core';
import { assets } from '../../../environments/assets';
import { BaseAssetService } from './base-asset.service';

@Injectable({ providedIn: 'root' })
export class AssetEnService extends BaseAssetService {
  public getUrl(key: string): string {
    const data = assets['en'];
    const value = data[key];
    return value;
  }
}

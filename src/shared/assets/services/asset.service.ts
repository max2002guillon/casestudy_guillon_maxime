import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { assets } from '../../../environments/assets';
import { AssetEnService } from './asset.en.service';
import { AssetFrService } from './asset.fr.service';
import { BaseAssetService } from './base-asset.service';

@Injectable({ providedIn: 'root', })
export class AssetService extends BaseAssetService {
  constructor(
    private readonly translateService: TranslateService,
    private readonly assetEnService: AssetEnService,
    private readonly assetFrService: AssetFrService,
  ) {
    super();
  }

  public getUrl(key: string): any {
    const data = assets;
    const value = data[key];
    return value;
  }

  public getI18nUrl(key: string): string {
    const lang = this.translateService.getDefaultLang();
    let service: BaseAssetService = this.assetEnService;
    if (lang === 'fr') {
      service = this.assetFrService;
    }
    const value = service.getUrl(key) || this.assetEnService.getUrl(key);
    return value;
  }
}

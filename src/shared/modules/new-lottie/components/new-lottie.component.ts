import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { AnimationItem } from 'lottie-web';
import { LottieComponent } from 'ngx-lottie';

@Component({
  selector: 'new-lottie',
  templateUrl: './new-lottie.component.html',
  styleUrls: ['./new-lottie.component.scss']
})
export class NewLottieComponent implements OnInit {
  @ViewChild('lottie') public base: LottieComponent;

  private animationItem: AnimationItem;

  constructor(private ngZone: NgZone) { }

  ngOnInit(): void {
  }

  animationCreated(animationItem: AnimationItem): void {
    this.animationItem = animationItem;
  }

  public stop(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationItem.stop();
    });
  }

  public play(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationItem.play();
    });
  }
}

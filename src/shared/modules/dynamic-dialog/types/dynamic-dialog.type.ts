export type DynamicDialogConfig<T = any> = {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
  content?: any;
  data?: T;
  width?: string | number;
  height?: string | number;
};

export type DynamicDialogCloseEvent = {
  type: 'backdropClick' | 'close';
  data: any;
 };

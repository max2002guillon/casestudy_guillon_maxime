import { Component, OnInit, TemplateRef } from '@angular/core';
import { DynamicDialogContent, DynamicDialogRef } from '../services/dynamic-dialog-ref';

@Component({
  selector: 'app-dynamic-dialog',
  templateUrl: './dynamic-dialog.component.html',
  styleUrls: ['./dynamic-dialog.component.scss']
})
export class DynamicDialogComponent implements OnInit {

  public renderMethod: 'template' | 'component' | 'text' = 'component';
  public content: DynamicDialogContent;
  public context;

  constructor(
    private dynamicDialogRef: DynamicDialogRef
  ) { }

  ngOnInit(): void {
    this.content = this.dynamicDialogRef.content;

    if (typeof this.content === 'string') {
      this.renderMethod = 'text';
    } else if (this.content instanceof TemplateRef) {
      this.renderMethod = 'template';
      this.context = {
        close: this.dynamicDialogRef.close.bind(this.dynamicDialogRef)
      };
    }
  }

}

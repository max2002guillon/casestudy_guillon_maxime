import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicDialogComponent } from './components/dynamic-dialog.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DynamicDialogComponent
  ],
  exports: [
    DynamicDialogComponent
  ]
})
export class DynamicDialogModule { }

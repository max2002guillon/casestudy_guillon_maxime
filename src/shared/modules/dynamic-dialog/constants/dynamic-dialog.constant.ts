export class CloseTypeConstant {
  public static readonly BACKDROP_CLICK = 'backdropClick';
  public static readonly CLOSE = 'close';
}

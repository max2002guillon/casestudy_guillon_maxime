import { OverlayRef } from '@angular/cdk/overlay';
import { Type, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';
import { CloseTypeConstant } from '../constants/dynamic-dialog.constant';
import { DynamicDialogCloseEvent } from '../types/dynamic-dialog.type';

export type DynamicDialogContent = TemplateRef<any> | Type<any> | string;

export class DynamicDialogRef<R = any, T = any> {
  private afterClosed = new Subject<DynamicDialogCloseEvent>();
  afterClosed$ = this.afterClosed.asObservable();

  constructor(
    public overlay: OverlayRef,
    public content: DynamicDialogContent | TemplateRef<any> | Type<any>,
    public data: unknown
  ) {
    overlay.backdropClick().subscribe(() => this._close(CloseTypeConstant.BACKDROP_CLICK, data));
  }

  close(data?: unknown) {
    this._close(CloseTypeConstant.CLOSE, data);
  }

  private _close(type, data) {
    this.overlay.dispose();
    this.afterClosed.next({
      type,
      data
    });
    this.afterClosed.complete();
  }
}

import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { ComponentRef, Injectable, Injector } from '@angular/core';
import { DynamicDialogConfig } from '../types/dynamic-dialog.type';
import { DynamicDialogRef } from './dynamic-dialog-ref';
import { PortalInjector } from './portal-injector';


const DEFAULT_CONFIG: Partial<DynamicDialogConfig> = {
  hasBackdrop: true,
  backdropClass: 'dark-backdrop',
  panelClass: 'dynamic-dialog-panel'
};

@Injectable({
  providedIn: 'root'
})
export class DynamicDialogService {

  constructor(
    private overlay: Overlay,
    private injector: Injector
  ) { }

  open<T>(component: ComponentType<T>, config: DynamicDialogConfig = {}) {
    const dialogConfig = { ...DEFAULT_CONFIG, ...config };
    const overlayRef = this.createOverlay(dialogConfig);
    const dialogRef = new DynamicDialogRef(overlayRef, config.content, config.data);
    this.attachModalContainer<T>(overlayRef, config, dialogRef, component);
    return dialogRef;
  }

  private createOverlay(config: DynamicDialogConfig): OverlayRef {
    const overlayConfig = this.getOverlayConfig(config);
    return this.overlay.create(overlayConfig);
  }

  private getOverlayConfig(config: DynamicDialogConfig): OverlayConfig {
    return new OverlayConfig({
      ...config,
      positionStrategy: this.getOverlayPosition(),
      scrollStrategy: this.overlay.scrollStrategies.block(),
    });
  }

  private getOverlayPosition(): PositionStrategy {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    return positionStrategy;
  }

  private attachModalContainer<T>(
    overlayRef: OverlayRef,
    config: DynamicDialogConfig,
    dialogRef: DynamicDialogRef,
    component: ComponentType<T>
  ) {
    const injector = this.createInjector(dialogRef);
    const containerPortal = new ComponentPortal(component, null, injector);
    const containerRef: ComponentRef<T> = overlayRef.attach(containerPortal);
    return containerRef.instance;
  }

  private createInjector(dialogRef: DynamicDialogRef): PortalInjector {
    const injectionTokens = new WeakMap();
    injectionTokens.set(DynamicDialogRef, dialogRef);
    return new PortalInjector(this.injector, injectionTokens);
  }
}

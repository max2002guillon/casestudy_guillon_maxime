import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { SpinnerService } from '../services/spinner.service';

export const InterceptorHideSpinnerLoadingHeader = 'X-Hide-Spinner-Loading-Interceptor';
export const HeaderHideSpinnerLoading = { headers: { [InterceptorHideSpinnerLoadingHeader]: '' } };

@Injectable({
  providedIn: 'root'
})
export class SpinnerInterceptor implements HttpInterceptor {
  constructor(
    private spinnerService: SpinnerService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const shouldShow = !req.headers.has(InterceptorHideSpinnerLoadingHeader);

    if (shouldShow) {
      this.showLoader();
    }

    const headers = req.headers.delete(InterceptorHideSpinnerLoadingHeader);
    return next.handle(req.clone({ headers })).pipe(
      finalize(() => this.onEnd()),
      tap((event: HttpEvent<any>) => {
        if (shouldShow) {
          if (event instanceof HttpResponse) {
            this.onEnd();
          }
        }
      }, () => {
        if (shouldShow) {
          this.onEnd();
        }
      }
      ));
  }

  private onEnd(): void {
    this.hideLoader();
  }

  private showLoader(): void {
    this.spinnerService.show();
  }

  private hideLoader(): void {
    this.spinnerService.hide();
  }
}

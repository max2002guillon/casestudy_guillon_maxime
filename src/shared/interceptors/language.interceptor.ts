import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';

export const InterceptorIgnoreLanguage = 'X-Ignore-Language-Interceptor';

@Injectable()
export class LanguageInterceptor implements HttpInterceptor {
  constructor(
    @Inject(LOCALE_ID) public locale: string,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const ignore = request.headers.has(InterceptorIgnoreLanguage);
    let headers = request.headers;
    headers = request.headers.delete(InterceptorIgnoreLanguage);

    if (!ignore && this.locale) {
      headers = headers.set('language', this.locale);
    }
    const changedRequest = request.clone({ headers });

    return next.handle(changedRequest);
  }
}

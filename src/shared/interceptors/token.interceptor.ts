import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUserService } from '../services/base-user.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private readonly userService: BaseUserService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const token = this.userService.getToken();
    const refreshToken = this.userService.getRefreshToken();

    let headers = request.headers;
    if (token) {
      const authorizationValue = 'Bearer ' + token;
      headers = headers.set('Authorization', authorizationValue);
    }
    if (refreshToken) {
      headers = headers.set('x-token', refreshToken);
    }

    const changedRequest = request.clone({ headers });
    return next.handle(changedRequest);
  }
}

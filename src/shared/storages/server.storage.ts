import { Inject, Injectable } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';

@Injectable()
export class ServerStorage implements Storage {
  [index: number]: string;
  [key: string]: any;
  length: number;
  private _cookies: any = [];

  constructor(@Inject(REQUEST) private request: any) {
    if (!this.request) {
      this._cookies = [];
      return;
    }
    this._cookies =
      (this.request.headers &&
        this.getAllExistCookies(this.request.headers.cookie)) ||
      [];
  }

  public clear(): void {
    this._cookies = [];
  }

  public getItem(key: string): string {
    return this._cookies[key];
  }

  public key(index: number): string {
    return this._cookies.propertyIsEnumerable[index];
  }

  public removeItem(key: string): void {
    this._cookies[key] = undefined;
  }

  public setItem(key: string, data: string): void {
    this._cookies[key] = data;
  }

  private getAllExistCookies(cookie: string): string[] {
    const cookies = [];
    if (cookie && cookie !== '') {
      const split = cookie.split(';');
      for (const row of split) {
        const currentCookie = row.split('=');
        currentCookie[0] = currentCookie[0].replace(/^ /, '');
        cookies[decodeURIComponent(currentCookie[0])] = decodeURIComponent(
          currentCookie[1]
        );
      }
    }
    return cookies;
  }
}

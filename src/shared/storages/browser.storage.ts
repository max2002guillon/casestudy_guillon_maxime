import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class BrowserStorage implements Storage {
  [index: number]: string;
  [key: string]: any;
  length: number;

  constructor(private readonly cookieService: CookieService) {}

  public clear(): void {
    this.cookieService.deleteAll('/');
  }

  public getItem(key: string): string | null {
    return this.cookieService.get(key);
  }

  public key(index: number): string | null {
    return this.cookieService.getAll().propertyIsEnumerable[index];
  }

  public removeItem(key: string): void {
    return this.cookieService.delete(key, '/');
  }

  public setItem(key: string, data: string): void {
    return this.cookieService.set(key, data, undefined, '/');
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Game1Component } from './game1/game1.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';

const routes: Routes = [
  {
    path: '', component: LoadingPageComponent
  },
  {
    path: 'welcome', component: WelcomePageComponent
  },
  {
    path:'game1', component: Game1Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

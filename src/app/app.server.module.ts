import { NgModule } from '@angular/core';
import {
  ServerModule,
  ServerTransferStateModule,
} from '@angular/platform-server';
import { ServerStorage } from '../shared/storages/server.storage';
import { STORAGE } from '../shared/storages/storages.inject';
import { TranslateServerModule } from '../shared/translate/translate-server.module';
import { AppModule } from './app.module';
import { AppComponent } from './components/app.component';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ServerTransferStateModule,
    TranslateServerModule,
  ],
  providers: [{ provide: STORAGE, useClass: ServerStorage }],
  bootstrap: [AppComponent],
})
export class AppServerModule {}

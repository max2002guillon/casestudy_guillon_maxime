import { isPlatformBrowser } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_ID, Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { environment } from '../environments/environment';
import { NgSwCheckForUpdateService } from '../shared/services/service-worker/ngsw-check-for-update.service';
import { NgSwLogUpdateService } from '../shared/services/service-worker/ngsw-log-update.service';
import { NgSwPromptUpdateService } from '../shared/services/service-worker/ngsw-prompt-update.service';
import { BrowserStorage } from '../shared/storages/browser.storage';
import { STORAGE } from '../shared/storages/storages.inject';
import { TranslateBrowserModule } from '../shared/translate/translate-browser.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';
import { Game1Component } from './game1/game1.component';

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'emeraude-games' }),
    TransferHttpCacheModule,
    TranslateBrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // Enable service worker.
    ServiceWorkerModule.register('./ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerImmediately',
    }),

    BrowserAnimationsModule,
  ],
  declarations: [
    AppComponent,
    WelcomePageComponent,
    LoadingPageComponent,
    Game1Component
   ],
  bootstrap: [AppComponent],
  providers: [
    NgSwLogUpdateService,
    NgSwPromptUpdateService,
    NgSwCheckForUpdateService,
    { provide: STORAGE, useClass: BrowserStorage },
  ],
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) platformId: object,
    @Inject(APP_ID) appId: string
  ) {
    const platform = isPlatformBrowser(platformId)
      ? 'in the browser'
      : 'on the server';
    console.info(`Running ${platform} with appId=${appId}`);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game1',
  templateUrl: './game1.component.html',
  styleUrls: ['./game1.component.scss']
})
export class Game1Component implements OnInit {

  constructor() { }

  game1backgroundURL: string;
  listPlatform: string[];
  tutoGame1URL: string;
  coolGuysURL: string;

  platformPosition: any[]

  bandeau: any;
  doodler: any;
  background: any;
  container: any;

  doodlerMarginTop: number;
  doodlerSpaceLeft: number;

  oldXpos: number;

  minX: number;
  maxX: number;

  inJump: boolean;

  pointOfStartJump: number;

  bottomScreen: number;

  canStart: boolean;

  posFalling: number;

  platformClass: any;


  ngOnInit(): void {
    this.game1backgroundURL = "../../assets/myPictures/backgroundGame1.svg";
    this.tutoGame1URL = "../../assets/myPictures/tutoGame1.svg";
    this.coolGuysURL = "../../assets/myPictures/coolGuys.svg";

    this.listPlatform = [
      "../../assets/myPictures/platforms/item1Game1.svg",
      "../../assets/myPictures/platforms/item2Game1.svg",
      "../../assets/myPictures/platforms/item3Game1.svg",
      "../../assets/myPictures/platforms/item4Game1.svg",
      "../../assets/myPictures/platforms/item5Game1.svg",
      "../../assets/myPictures/platforms/item6Game1.svg",
      "../../assets/myPictures/platforms/item7Game1.svg",
      "../../assets/myPictures/platforms/item8Game1.svg",
      "../../assets/myPictures/platforms/item9Game1.svg",
      "../../assets/myPictures/platforms/item10Game1.svg",
      "../../assets/myPictures/platforms/item11Game1.svg",
      "../../assets/myPictures/platforms/item12Game1.svg",
      "../../assets/myPictures/platforms/item13Game1.svg",
      "../../assets/myPictures/platforms/item14Game1.svg",
      "../../assets/myPictures/platforms/item15Game1.svg",
      "../../assets/myPictures/platforms/item16Game1.svg",
      "../../assets/myPictures/platforms/item17Game1.svg",
      "../../assets/myPictures/platforms/item18Game1.svg",
      "../../assets/myPictures/platforms/item19Game1.svg",
      "../../assets/myPictures/platforms/item20Game1.svg",
      "../../assets/myPictures/platforms/item21Game1.svg"
    ]

    this.minX = 5;
    this.maxX = 75;

    this.oldXpos = 35;

    this.inJump = false;

    this.pointOfStartJump = 0;


    this.platformPosition = [
      { bottom: 335, left: 35 },
      { bottom: 320, left: this.randomLeft() },
      { bottom: 305, left: this.randomLeft() },
      { bottom: 290, left: this.randomLeft() },
      { bottom: 275, left: this.randomLeft() },
      { bottom: 260, left: this.randomLeft() },
      { bottom: 245, left: this.randomLeft() },
      { bottom: 230, left: this.randomLeft() },
      { bottom: 215, left: this.randomLeft() },
      { bottom: 200, left: this.randomLeft() },
      { bottom: 185, left: this.randomLeft() },
      { bottom: 170, left: this.randomLeft() },
      { bottom: 155, left: this.randomLeft() },
      { bottom: 140, left: this.randomLeft() },
      { bottom: 125, left: this.randomLeft() },
      { bottom: 110, left: this.randomLeft() },
      { bottom: 95, left: this.randomLeft() },
      { bottom: 80, left: this.randomLeft() },
      { bottom: 55, left: this.randomLeft() },
      { bottom: 50, left: this.randomLeft() },
      { bottom: 35, left: this.randomLeft() },
    ]



    this.bandeau = document.getElementById('bandeau');
    this.doodler = document.getElementById('doodler');
    this.background = document.getElementById('background');
    this.container = document.getElementById('container');


    this.doodlerMarginTop = 320;
    this.doodlerSpaceLeft = 40;

    this.doodler.style.marginTop = this.doodlerMarginTop + "vw";
    this.doodler.style.left = this.doodlerSpaceLeft + "vw";

    this.bottomScreen = 0;

    this.canStart = false;

    this.posFalling = -292;

    this.platformClass = document.getElementsByClassName("platform")

    this.update();

  }
  launchGame(): void {
    this.bandeau.setAttribute('hidden', true);
    this.doodler.style.marginTop = this.doodlerMarginTop + "vw";
    this.canStart = true;


  }

  jump(): void {
    this.doodlerMarginTop -= 0.5;
    this.doodler.style.marginTop = this.doodlerMarginTop + "vw";
  }

  fall(): void {
    this.doodlerMarginTop += 0.5;
    this.doodler.style.marginTop = this.doodlerMarginTop + "vw";
  }

  update() {

    if (this.canStart) {
      if (this.detectCollision()) {
        this.inJump = true;
        this.pointOfStartJump = this.doodlerMarginTop;
      }
      if (this.inJump) {
        if (this.maxJump()) {
          this.jump()

        }
        else {
          this.inJump = false;
        }
      }
      else {
        this.fall();
      }
      this.detectKeyPress();
      this.screenFall();
    }

    requestAnimationFrame(() => this.update());
  }

  randomLeft(): number {
    var newX = Math.random() * ((this.oldXpos + 35 > this.maxX ? this.maxX : this.oldXpos + 35) - (this.oldXpos - 35 < this.minX ? this.minX : this.oldXpos - 35)) + (this.oldXpos - 35 < this.minX ? this.minX : this.oldXpos - 35);
    this.oldXpos = newX;

    return newX;
  }

  maxJump(): boolean {
    if (this.pointOfStartJump - 30 > this.doodlerMarginTop) {
      return false;
    }

    return true;

  }

  detectCollision(): boolean {

    for (let i = 0; i < this.platformPosition.length; i++) {

      const platform = this.platformPosition[i];

      const platformBottom = platform.bottom;
      const platformLeft = platform.left;

      if (this.doodlerMarginTop + 5 == platformBottom && this.doodlerSpaceLeft >= platformLeft - 5 && this.doodlerSpaceLeft <= platformLeft + 10) {
        this.pointOfStartJump = this.doodlerMarginTop;
        return true;
      }
    }
    return false;
  }

  detectKeyPress(): void {
    document.addEventListener('keydown', (event) => {
      if (event.key === 'ArrowLeft') {
        if (this.doodlerSpaceLeft > this.minX) {
          this.doodlerSpaceLeft -= 0.01;
          this.doodler.style.left = this.doodlerSpaceLeft + "vw";
        }

      }
      if (event.key === 'ArrowRight') {
        if (this.doodlerSpaceLeft < this.maxX) {
          this.doodlerSpaceLeft += 0.01;
          this.doodler.style.left = this.doodlerSpaceLeft + "vw";
        }
      }
    })
  }

  screenFall(): void {
    if (this.posFalling < 0) {
      this.posFalling += 0.1;
    }

    this.container.style.marginTop = this.posFalling + "vw";

    this.platformClass.marginTop = this.posFalling + "vw";

  }

}








import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AppService {
  public isIE() {
    const ua = navigator.userAgent;
    const isIE = !!ua.match(/Trident/g) || !!ua.match(/MSIE/g);
    return isIE;
  }
}

import { AssetKeyConstant as SharedAssetKeyConstant } from '../../shared/constants/asset-key.constant';

export class AssetKeyConstant extends SharedAssetKeyConstant {
  /* ----------------------------------------- */
  /*                SCENE1                     */
  /* ----------------------------------------- */
  // opening
  public static readonly SCENE1_OPENING_FRAME_URL = 'SCENE1_OPENING_FRAME_URL';
  public static readonly SCENE1_OPENING_BACKGROUND_URL = 'SCENE1_OPENING_BACKGROUND_URL';
  public static readonly SCENE1_OPENING_MOBILE_FRAME_URL = 'SCENE1_OPENING_MOBILE_FRAME_URL';
  public static readonly SCENE1_OPENING_MOBILE_BACKGROUND_URL = 'SCENE1_OPENING_MOBILE_BACKGROUND_URL';

  // puzzle 1
  public static readonly SCENE1_PUZZLE1_FRAME_URL = 'SCENE1_PUZZLE1_FRAME_URL';
  public static readonly SCENE1_PUZZLE1_FRAME_MOBILE_URL = 'SCENE1_PUZZLE1_FRAME_MOBILE_URL';
  public static readonly SCENE1_PUZZLE1_BACKGROUND_URL = 'SCENE1_PUZZLE1_BACKGROUND_URL';
  public static readonly SCENE1_PUZZLE1_BACKGROUND_MOBILE_URL = 'SCENE1_PUZZLE1_BACKGROUND_MOBILE_URL';

  // puzzle2
  public static readonly SCENE1_PUZZLE2_FRAME_URL = 'SCENE1_PUZZLE2_FRAME_URL';
  public static readonly SCENE1_PUZZLE2_BACKGROUND_URL = 'SCENE1_PUZZLE2_BACKGROUND_URL';
  public static readonly SCENE1_PUZZLE2_MOBILE_FRAME_URL = 'SCENE1_PUZZLE2_MOBILE_FRAME_URL';
  public static readonly SCENE1_PUZZLE2_MOBILE_BACKGROUND_URL = 'SCENE1_PUZZLE2_MOBILE_BACKGROUND_URL';

  // puzzle3
  public static readonly SCENE1_PUZZLE3_FRAME_DESKTOP_URL = 'SCENE1_PUZZLE3_FRAME_DESKTOP_URL';
  public static readonly SCENE1_PUZZLE3_FRAME_MOBILE_URL = 'SCENE1_PUZZLE3_FRAME_MOBILE_URL';
}

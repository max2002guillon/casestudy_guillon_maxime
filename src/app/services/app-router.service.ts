import { Injectable } from '@angular/core';
import { Event, NavigationExtras, NavigationStart, Router, UrlTree } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AppRouterService {
  public get events() {
    return this.router.events;
  }

  constructor(private readonly router: Router) { }

  public navigate(commands: any[], extras?: NavigationExtras) {
    if (!extras) {
      extras = { skipLocationChange: true };
    }
    return this.router.navigate(commands, extras);
  }

  public navigateByUrl(url: string | UrlTree, extras?: NavigationExtras) {
    if (!extras) {
      extras = { skipLocationChange: true };
    }
    return this.router.navigateByUrl(url, extras);
  }

  public getNavigationStartObservable() {
    // https://angular.io/api/router/RouterEvent#description
    return this.events.pipe(filter((ev: Event): ev is NavigationStart => ev instanceof NavigationStart));
  }
}

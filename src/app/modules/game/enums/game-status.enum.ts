export enum GameStatus {
  STARTED = 'STARTED',
  COMPLETED = 'COMPLETED',
  SKIPPED = 'SKIPPED',
}

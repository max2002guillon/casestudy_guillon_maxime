import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './components/game/game.component';

const routes: Routes = [
  {
    path: '',
    component: GameComponent,
    children: [
      {
        path: '',
        redirectTo: 'tutorial',
        pathMatch: 'full'
      },
      {
        path: 'tutorial',
        loadChildren: () => import('../../modules/game/tutorial/tutorial.module').then((m) => m.TutorialModule),
      },
      {
        path: 'scene1',
        loadChildren: () => import('../../modules/game/scene1/scene1.module').then((m) => m.Scene1Module),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameRoutingModule { }

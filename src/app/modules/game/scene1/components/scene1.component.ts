import { Component } from '@angular/core';
import { BaseComponent } from '../../../../../shared/components/base-component/base.component';
import { GameBoardService } from '../../../../../shared/services/game-board.service';


@Component({
  selector: 'app-scene1',
  templateUrl: './scene1.component.html',
})
export class Scene1Component extends BaseComponent {
  constructor(
    private readonly gameBoardService: GameBoardService,
  ) {
    super();
  }

  async onInit() {
    await this.gameBoardService.loadScene('/assets/images/game-map-opening-fg.svg', 'https://storage.googleapis.com/emeraude-dyptique-production.appspot.com/public/assets/images/map/game-map-opening-bg.png');
  }
}

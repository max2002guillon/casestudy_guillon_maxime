import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseGameControlService } from '../../../../../shared/services/base-game-control.service';
import { GameKeyService, ListKey } from './../../../../../shared/services/game-key.service';
import { GameUtil } from './../../../../../shared/utils/game.util';

@Injectable()
export class Scene1Service extends BaseGameControlService {
  private readonly ITEM_CLE_ID = 'item-cle';
  private readonly ITEM_CLE2_ID = 'item-cle2';

  constructor(
    protected readonly translateService: TranslateService,
    private readonly gameKeyService: GameKeyService
  ) {
    super();
  }

  public  collectOrangeKey(): void {
    this.gameKeyService.increaseNumbOfKey(ListKey.ORANGE);
    GameUtil.hideElementById(this.ITEM_CLE_ID);
  }

  public collectGreenKey(): void {
    this.gameKeyService.increaseNumbOfKey(ListKey.GREEN);
    GameUtil.hideElementById(this.ITEM_CLE2_ID);
  }
}

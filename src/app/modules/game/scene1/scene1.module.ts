import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { DragScrollModule } from 'ngx-drag-scroll';
import { CodeInputModule } from '../../../../shared/components/code-input/code-input.module';
import { Scene1Component } from './components/scene1.component';
import { Scene1RoutingModule } from './scene1-routing.module';
import { Scene1Service } from './services/scene1.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    DragDropModule,
    DragScrollModule,
    Scene1RoutingModule,
    CodeInputModule,
  ],
  declarations: [
    Scene1Component,
  ],
  providers: [
    Scene1Service,
  ],
})
export class Scene1Module { }

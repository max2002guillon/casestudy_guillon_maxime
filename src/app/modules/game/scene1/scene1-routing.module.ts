import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreloadResolver } from '../../../../shared/services/preload.resolver';
import { Scene1Component } from './components/scene1.component';

const routes: Routes = [
  {
    path: '',
    component: Scene1Component,
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PreloadResolver]
})
export class Scene1RoutingModule { }

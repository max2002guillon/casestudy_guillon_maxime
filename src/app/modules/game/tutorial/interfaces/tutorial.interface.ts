import { AnimationOptions } from 'ngx-lottie';
import { ElementPosition } from '../../../../../shared/interfaces/game-permanent-element.interface';

export interface ITutorialSetting {
  background: ITutorialBackground;
  assetsInOrder: ITutorialAsset[];
}

export interface ITutorialAsset {
  clickZone: ITutorialClickZone[];
  description: ITutorialDescription;
  buttonNext: ITutorialBtnNext;
  isEnable: boolean;
}

export interface ITutorialBackground {
  cssClass: string;
  blackArea: AssetBackground[];
  imageBackground?: AssetBackground;
  animation?: {
    configLottie?: AnimationOptions;
    id: string;
    url: string;
    cssClass: string;
  };
}
export interface AssetBackground {
  id?: string;
  url?: string;
  cssClass?: string;
  position?: ElementPosition;
  label?: string;
}

export interface ITutorialClickZone {
  id?: string;
  permanentType: PermanentElementType;
  position?: ElementPosition;
  cssClass?: string;
  customEvent?: () => {};
}

export interface ITutorialDescription {
  title: string;
  content: string;
  cssClass: string;
  arrowImageUrl?: string;
  arrowCssClass?: string;
}

export interface ITutorialBtnNext {
  id: string;
  text: string;
  cssClass: string;
  animation?: {
    url: string;
    usingLottie: boolean;
  };
  customEvent?: () => {};
}

export enum PermanentElementType {
  TIMER = 'TIMER',
  SOUND = 'SOUND',
  INVENTORY = 'INVENTORY',
  INVENTORY_OPEN = 'INVENTORY_OPEN',
  CHATBOT = 'CHATBOT',
  PROGRESS = 'PROGRESS',
  NAVIGATION_ARROW = 'NAVIGATION_ARROW',
  MOUSE = 'MOUSE',
  BONUS_PENALTY = 'BONUS_PENALTY',
  BACK_BTN = 'BACK_BTN',
  WEBCAMS = 'WEBCAMS',
  KEY = 'KEY'
}

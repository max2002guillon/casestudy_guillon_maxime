import { PermanentElementType } from './tutorial.interface';

export interface TutorialConfigSettingAPIModel {
  permanentInOrder: PermanentElementType[];
  background: {
    style: unknown;
    imageBackground: {
      url: string;
      style: unknown;
    },
    blackArea: {
      style: unknown;
    }
  };
  description: {
    style: unknown;
    title: {
      style: unknown;
    };
    content: {
      style: unknown;
    }
  };
  buttonNext: {
    style: unknown;
    textStyle: unknown;
  };
}

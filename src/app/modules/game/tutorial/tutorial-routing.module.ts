import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TutorialComponent } from './components/tutorial.component';
import { TutorialResolver } from './resolver/tutorial.resolver';

const routes: Routes = [
  {
    path: '',
    component: TutorialComponent,
    resolve: {
      configSettings: TutorialResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TutorialRoutingModule { }

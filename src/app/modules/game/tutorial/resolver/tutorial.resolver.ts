import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { TutorialService } from '../services/tutorial.service';

@Injectable()
export class TutorialResolver implements Resolve<unknown> {
  constructor(
    private readonly tutorialService: TutorialService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.tutorialService.getConfigTutorialActive().pipe(
      mergeMap((isActive: boolean) => {
        return isActive ? this.tutorialService.getConfigurationSetting() : of({});
      }));
  }
}

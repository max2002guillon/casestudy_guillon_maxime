import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, timer } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GameNavigationBackService } from '../../../../../shared/components/game-navigation-back/services/game-navigation-back.service';
import { PermanentElementID } from '../../../../../shared/constants/permanent-element.constant';
import { GameSoundService } from '../../../../../shared/services/audio/game-sound.service';
import { BaseGameControlService } from '../../../../../shared/services/base-game-control.service';
import { GameComponentService } from '../../../../../shared/services/game-component.service';
import { GameInventoryService } from '../../../../../shared/services/game-inventory.service';
import { GameKeyService } from '../../../../../shared/services/game-key.service';
import { GameNavigationService } from '../../../../../shared/services/game-navigation.service';
import { GamePermanentElementService } from '../../../../../shared/services/game-permanent-element.service';
import { GameTimerService } from '../../../../../shared/services/game-timer.service';
import { TutorialConfiguration } from '../configurations/tutorial.configuration';
import { TutorialConfigSettingAPIModel } from '../interfaces/config-settings.interface';
import { ITutorialAsset, ITutorialBackground, ITutorialSetting, PermanentElementType } from '../interfaces/tutorial.interface';
import { TutorialAPIService } from './tutorial-api.service';

@Injectable()
export class TutorialService extends BaseGameControlService {

  private _tutorialSetting: ITutorialSetting = null;
  private elementServices: GameComponentService[] = [];
  private elementIds: string[] = [];
  private configSettings: TutorialConfigSettingAPIModel;
  public currentStepIndex = 0;
  public currentElementService: GameComponentService;

  constructor(
    private readonly tutorialConfiguration: TutorialConfiguration,
    private readonly gameTimerService: GameTimerService,
    private readonly gameNavigationService: GameNavigationService,
    private readonly gameInventoryService: GameInventoryService,
    private readonly gameNavigationBackService: GameNavigationBackService,
    private readonly gameSoundService: GameSoundService,
    private readonly gameKeyService: GameKeyService,
    private readonly router: Router,
    public gamePermanentElementService: GamePermanentElementService,
    private readonly tutorialAPIService: TutorialAPIService
  ) {
    super();
  }

  public get assetsInOrder(): ITutorialAsset[] {
    return this._tutorialSetting.assetsInOrder;
  }

  public set assetsInOrder(value: ITutorialAsset[]) {
    this._tutorialSetting.assetsInOrder = value;
  }

  public get tutorialSetting(): ITutorialSetting {
    return this._tutorialSetting;
  }

  public set tutorialSetting(value: ITutorialSetting) {
    this._tutorialSetting = value;
  }

  public get tutorialAsset(): ITutorialAsset {
    return this._tutorialSetting.assetsInOrder[this.currentStepIndex];
  }

  public get elementId(): string {
    return this.elementIds[this.currentStepIndex];
  }

  public init(configSettings: TutorialConfigSettingAPIModel): void {
    this.configSettings = configSettings;
    const background: ITutorialBackground = {
      ...this.tutorialConfiguration.getBackgroundConfiguration(),
      imageBackground: {
        ...this.tutorialConfiguration.getBackgroundConfiguration().imageBackground,
        ...configSettings.background.imageBackground
      }
    };
    const assetsInOrder = this.tutorialConfiguration.getAssetsConfiguration(this.configSettings.permanentInOrder);
    this.tutorialSetting = {
      background,
      assetsInOrder
    };
    this.setElementServiceInOrder();

    this.elementServices.forEach(s => {
      s.setVisibility(false);
    });

    this.currentElementService = this.elementServices[0];
    this.currentElementService.setVisibility(true);
  }

  public listenElementPositionChange(): void {
    timer(400).subscribe(() => {
      this.gamePermanentElementService.listenElementPositionChange(this.elementIds[this.currentStepIndex]);
    });
  }

  public enableAssetByStep(): void {
    this.assetsInOrder = this.assetsInOrder.map(a => {
      return {
        ...a,
        isEnable: false
      };
    });
    this.assetsInOrder[this.currentStepIndex].isEnable = true;
  }

  public getNumberOfStep(): number {
    return this.tutorialConfiguration.NUMB_OF_STEPS;
  }

  public getCurrentStepIndex(): number {
    return this.currentStepIndex;
  }

  public moveToNextStep(): void {
    if (this.currentStepIndex < this.elementIds.length - 1) {
      this.currentElementService.setVisibility(false);
      this.currentStepIndex = this.currentStepIndex + 1;
      this.currentElementService = this.elementServices[this.currentStepIndex];
      this.currentElementService.setVisibility(true);
      this.gamePermanentElementService.listenElementPositionChange(this.elementIds[this.currentStepIndex]);
    } else {
      this.currentElementService.setVisibility(false);
      this.router.navigate(this.tutorialConfiguration.NEXT_ROUTE_URL);
    }
  }

  public listenWindowResize(): void {
    window.addEventListener('resize', (event) => {
      this.gamePermanentElementService.listenElementPositionChange(this.elementIds[this.currentStepIndex]);
    });
  }

  public resizeDragScrollComponent(): void {
    timer(1000).subscribe(() => {
      const dragScrollContent = document.querySelector('.drag-scroll-content') as HTMLElement;
      dragScrollContent.style.width = '100%';
    });
  }

  private setElementServiceInOrder(): void {
    const elementTypesInOrder = this.tutorialConfiguration.elements;
    for (const elementType of elementTypesInOrder) {
      switch (elementType) {
        case PermanentElementType.TIMER:
          this.elementServices.push(this.gameTimerService);
          this.elementIds.push(PermanentElementID.TIMER);
          break;
        case PermanentElementType.NAVIGATION_ARROW:
          this.elementServices.push(this.gameNavigationService);
          this.elementIds.push(PermanentElementID.NAVIGATION_ARROW);
          break;
        case PermanentElementType.CHATBOT:
          // this.elementServices.push(this.gameChatbotService);
          // this.elementIds.push(PermanentElementID.CHATBOT);
          break;
        case PermanentElementType.INVENTORY:
          this.elementServices.push(this.gameInventoryService);
          this.elementIds.push(PermanentElementID.INVENTORY);
          break;
        case PermanentElementType.PROGRESS:
          break;
        case PermanentElementType.BACK_BTN:
          this.elementServices.push(this.gameNavigationBackService);
          this.elementIds.push(PermanentElementID.BACK_BTN);
          break;
        case PermanentElementType.SOUND:
          this.elementServices.push(this.gameSoundService);
          this.elementIds.push(PermanentElementID.SOUND);
          break;
        case PermanentElementType.KEY:
          this.elementServices.push(this.gameKeyService);
          this.elementIds.push(PermanentElementID.KEY);
          break;

        default:
          break;
      }
    }
  }

  public getConfigurationSetting(): Observable<unknown> {
    return this.tutorialAPIService.getConfigTutorialData().pipe(
      map(result => result as unknown),
      catchError((err) => {
        throw err;
      })
    );
  }

  public getConfigTutorialActive(): Observable<boolean> {
    return this.tutorialAPIService.getConfigTutorialActive().pipe(
      map(result => result as boolean),
      catchError((err) => {
        throw err;
      })
    );
  }
}

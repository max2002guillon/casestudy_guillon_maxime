import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TutorialAPIService {
  private baseUrl: string = environment.apiUrl;

  constructor(
    private readonly http: HttpClient,
    private readonly transferState: TransferState,
  ) { }

  public getConfigTutorialActive() {
    const url = `${this.baseUrl}/config/tutorial/active`;
    this.clearCache(url);
    return this.http.get(url);
  }

  public getConfigTutorialData(): Observable<unknown> {
    const url = `${this.baseUrl}/config/tutorial/data`;
    this.clearCache(url);
    return this.http.get(url);
  }

  private clearCache(url: string): void {
    try {
      const obj = JSON.parse(this.transferState.toJson());
      Object.keys(obj).forEach((key: string) => {
        const value = obj[key];
        if (value.url !== url) {
          return;
        }

        const stateKey = makeStateKey(key);
        this.transferState.remove(stateKey);
      });
    } catch (error) {
      throw error;
    }
  }


}

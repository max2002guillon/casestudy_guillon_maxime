import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import player from 'lottie-web';
import { LottieModule } from 'ngx-lottie';
import { TutorialComponent } from './components/tutorial.component';
import { TutorialConfiguration } from './configurations/tutorial.configuration';
import { TutorialResolver } from './resolver/tutorial.resolver';
import { TutorialService } from './services/tutorial.service';
import { TutorialRoutingModule } from './tutorial-routing.module';

export function playerFactory() {
  return player;
}
@NgModule({
  imports: [
    CommonModule,
    TutorialRoutingModule,
    LottieModule.forRoot({ player: playerFactory })
  ],
  declarations: [
    TutorialComponent
  ],
  providers: [
    TutorialConfiguration,
    TutorialResolver,
    TutorialService
  ]
})
export class TutorialModule { }

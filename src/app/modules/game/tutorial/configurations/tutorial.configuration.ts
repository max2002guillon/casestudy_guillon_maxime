import { Injectable } from '@angular/core';
import {
  ITutorialAsset,
  ITutorialBackground, ITutorialBtnNext, ITutorialClickZone, ITutorialDescription, PermanentElementType
} from '../interfaces/tutorial.interface';

@Injectable({ providedIn: 'platform' })
export class TutorialConfiguration {
  public readonly NUMB_OF_STEPS = 6;
  public readonly NEXT_ROUTE_URL = ['game', 'scene1'];

  private readonly CLICK_ZONE_ID_PREFIX = 'item-clickable-zone';
  private readonly CLICK_ZONE_CLASS_PREFIX = 'tutorial-click-zone';

  private readonly DESCRIPTION_TITLE_PREFIX = 'TUTORIAL_TITLE_STEP';
  private readonly DESCRIPTION_CONTENT_PREFIX = 'TUTORIAL_CONTENT_STEP';
  public readonly DESCRIPTION_CLASS_PREFIX = 'tutorial-description';

  public readonly BACKGROUND_CLASS_PREFIX = 'tutorial-background';
  public readonly BACKGROUND_BLACK_AREA_CLASS_PREFIX = 'tutorial-black-area';
  private readonly BACKGROUND_BLACK_AREA_ID = 'tutorial-black-area-id';

  public readonly BACKGROUND_IMAGE_BACKGROUND_CLASS_PREFIX = 'image-background';

  private readonly ANIMATION_ASSET_PATH = 'assets/public-resources/images/tutorial/animation/tutorial-mouse-animation.svg';
  private readonly ANIMATION_STEP_PREFIX = 'tutorial-animation-step';
  private readonly ANIMATION_ID = 'tutorial-animation-id';

  private readonly BUTTON_TUTORIAL_ID = 'button-tutorial-id';
  private readonly BUTTON_TUTORIAL_TEXT = 'next';
  private readonly BUTTON_TUTORIAL_CLASS = 'button button--tutorial';

  constructor() {
  }

  public getBackgroundConfiguration(): ITutorialBackground {
    const background: ITutorialBackground = {
      cssClass: this.BACKGROUND_CLASS_PREFIX,
      animation: {
        // configLottie: this.ANIMATION_LOTTIE_CONFIG,
        id: this.ANIMATION_ID,
        url: this.ANIMATION_ASSET_PATH,
        cssClass: this.ANIMATION_STEP_PREFIX
      },
      // imageBackground: {
      //   url: this.BACKGROUND_ASSET_PATH,
      //   cssClass: this.BACKGROUND_IMAGE_BACKGROUND_CLASS
      // },
      imageBackground: null,
      blackArea: [{
        id: this.BACKGROUND_BLACK_AREA_ID,
        cssClass: this.BACKGROUND_BLACK_AREA_CLASS_PREFIX
      }],
    };
    return background;
  }


  public getAssetsConfiguration(elements: PermanentElementType[]): ITutorialAsset[] {
    const assetsInOrder: ITutorialAsset[] = [];
    for (let no = 1; no <= this.NUMB_OF_STEPS; no++) {

      const clickZone: ITutorialClickZone = {
        id: `${this.CLICK_ZONE_ID_PREFIX}-${no}`,
        cssClass: `${this.CLICK_ZONE_CLASS_PREFIX}-${no}`,
        permanentType: elements[no - 1]
      };

      const description: ITutorialDescription = {
        title: `${this.DESCRIPTION_TITLE_PREFIX}_${no}`,
        content: `${this.DESCRIPTION_CONTENT_PREFIX}_${no}`,
        cssClass: `${this.DESCRIPTION_CLASS_PREFIX}-${no}`
      };

      const buttonNext: ITutorialBtnNext = {
        id: `${this.BUTTON_TUTORIAL_ID}_${no}`,
        text: `${this.BUTTON_TUTORIAL_TEXT}`,
        cssClass: ` ${this.BUTTON_TUTORIAL_CLASS}_${no}`
      };

      const tutorialAsset: ITutorialAsset = {
        buttonNext,
        clickZone: [clickZone],
        description,
        isEnable: false
      };

      assetsInOrder.push(tutorialAsset);
    }
    return assetsInOrder;
  }

  // Get permanent elements in order
  public get elements(): PermanentElementType[] {
    return [
      PermanentElementType.KEY,
      PermanentElementType.TIMER,
      PermanentElementType.CHATBOT,
      PermanentElementType.NAVIGATION_ARROW,
      PermanentElementType.INVENTORY,
      PermanentElementType.INVENTORY_OPEN,
      PermanentElementType.PROGRESS,
      PermanentElementType.BACK_BTN,
      PermanentElementType.SOUND,
      PermanentElementType.WEBCAMS,
    ];
  }
}

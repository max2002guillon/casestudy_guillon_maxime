import { Component, HostBinding, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BaseComponent } from '../../../../../shared/components/base-component/base.component';
import { TutorialConfigSettingAPIModel } from '../interfaces/config-settings.interface';
import { ITutorialAsset, ITutorialBackground } from '../interfaces/tutorial.interface';
import { TutorialService } from '../services/tutorial.service';
import { PermanentElementPosition } from './../../../../../shared/interfaces/game-permanent-element.interface';
import { AppRouterService } from './../../../../services/app-router.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss'],
})
export class TutorialComponent extends BaseComponent {
  @ViewChild('viewContainerRef', { read: ViewContainerRef, static: true }) viewContainerRef: ViewContainerRef;
  @HostBinding('style.width') width = '100vw';

  public configSettings: TutorialConfigSettingAPIModel = null;

  public backgroundStyle = {};
  public backgroundImageStyle = {};

  constructor(
    private readonly tutorialService: TutorialService,
    private readonly activeRoute: ActivatedRoute,
    private readonly router: AppRouterService
  ) {
    super();
  }

  public handelAction($event: any): void {
    this.moveToNextStep();
  }

  public moveToNextStep(): void {
    this.tutorialService.moveToNextStep();
  }

  public get background(): ITutorialBackground {
    return this.tutorialService.tutorialSetting.background;
  }

  public get asset(): ITutorialAsset {
    return this.tutorialService.tutorialAsset;
  }

  onInit(): void {
    this.configSettings = this.activeRoute.snapshot.data.configSettings;
    if (Object.keys(this.configSettings).length === 0) {
      // TODO: Add Continue game
      this.router.navigate(['game', 'scene1']);
      return;
    }
    this.tutorialService.init(this.configSettings);
    this.initSubscribe();
    this.tutorialService.listenWindowResize();
  }

  onAfterViewInit(): void {
    this.tutorialService.listenElementPositionChange();
    this.tutorialService.resizeDragScrollComponent();
  }

  private initSubscribe() {
    const obs$ = this.tutorialService.gamePermanentElementService.getElementPosition$();
    this.handleClickAreaWhenPositionElementChange(obs$);
  }

  private handleClickAreaWhenPositionElementChange(obs$: Observable<PermanentElementPosition[]>): void {
    this.subscribe(obs$, (positions: PermanentElementPosition[]) => {
      if (!positions) {
        return null;
      }
      // this.currentPosition = position;

      if (positions[0].elementId.indexOf(this.tutorialService.elementId) !== -1) {
        this.resetAreaArray();
        const areaPositions = this.getBlackAreaPosition(positions);

        for (let index = 0; index < positions.length; index++) {
          if (index >= 1 && !this.background.blackArea[index]) {
            this.background.blackArea.push({
              id: `${this.background.blackArea[0].id}-${index}`,
              cssClass: this.background.blackArea[0].cssClass,
              position: areaPositions[index]
            });
            this.asset.clickZone.push({
              id: areaPositions[index].elementId,
              position: areaPositions[index],
              permanentType: this.asset.clickZone[0].permanentType,
              cssClass: this.asset.clickZone[0].cssClass,
            });
          } else {
            this.background.blackArea[index].position = areaPositions[index];
            this.asset.clickZone[index].position = areaPositions[index];
          }
        }
      }
    });
  }

  private getBlackAreaPosition(elementPosition: PermanentElementPosition[]): PermanentElementPosition[] {
    elementPosition = elementPosition.map(e => {
      return {
        elementId: e.elementId,
        top: e.top - 2,
        right: e.right - 2,
        left: e.left - 2,
        bottom: e.bottom - 2
      };
    });
    return elementPosition;
  }

  private resetAreaArray(): void {
    this.background.blackArea = [this.background.blackArea[0]];
    this.asset.clickZone = [this.asset.clickZone[0]];
  }

  // private addPositionElement(element: HTMLElement, position: PermanentElementPosition) {
  //   element.style.top = `${position.top}%`;
  //   element.style.bottom = `${position.bottom}%`;
  //   element.style.right = `${position.right}%`;
  //   element.style.left = `${position.left}%`;
  // }

  // private handleCreateAnimation(indexStep: number): void {
  //   if (this.background.animation) {
  //     if (this.background.animation.configLottie) {
  //       // TODO: apple animation lottie
  //       this.handleAnimationLottie();
  //       return;
  //     }
  //     const el = this.document.getElementById(this.background.animation.id);
  //     const url = this.background.animation.url;
  //     const cssClassByStep = `${this.background.animation.cssClass} ${indexStep}`;

  //     const elAnimation: HTMLElement = el ? el : this.renderImage(url, cssClassByStep);

  //     // TODO: add position class or get calc position permanent element
  //     const position = {
  //       ... this.currentPosition,
  //       left: (100 - this.currentPosition.left - this.currentPosition.right) / 2
  //     };
  //     this.addPositionElement(elAnimation, position);
  //     if (elAnimation) {
  //       this.render2.appendChild(this.bgSection, elAnimation);
  //     }
  //   }
  // }

  // private renderImage(url: string, cssClass: string): HTMLImageElement {
  //   const image: HTMLImageElement = this.render2.createElement('img');
  //   image.src = url;
  //   image.className = cssClass;
  //   return image;
  // }

  // private handleAnimationLottie(): void {
  //   const componentFactory = this.componentFactoryResolver.resolveComponentFactory(LottieComponent);
  //   const componentRef = this.viewContainerRef.createComponent(componentFactory);
  //   componentRef.onDestroy(() => this.viewContainerRef.remove());

  //   const options = this.background.animation.configLottie;
  //   const component = componentRef.instance;
  //   component.ngOnChanges({
  //     options: {
  //       previousValue: null,
  //       currentValue: options,
  //       firstChange: true,
  //       isFirstChange: () => true
  //     }
  //   });

  //   // TODO: calculator position and set size
  //   const size = {
  //     width: '50%',
  //     height: '10%'
  //   };

  //   component.styles = {
  //     position: 'absolute',
  //     pointerEvents: 'none',
  //     top: `${this.currentPosition.top}%`,
  //     bottom: `${this.currentPosition.bottom}%`,
  //     right: `${this.currentPosition.right}%`,
  //     left: `${this.currentPosition.left}%`,
  //     width: size.width,
  //     height: size.height,
  //   };
  // }
}

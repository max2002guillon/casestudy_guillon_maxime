import { Component, HostBinding } from '@angular/core';
import { BaseComponent } from '../../../../../shared/components/base-component/base.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent extends BaseComponent {
  @HostBinding('style.height') height = '100%';
}

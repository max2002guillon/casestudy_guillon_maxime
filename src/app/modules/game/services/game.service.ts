import { Inject, Injectable } from '@angular/core';
import { of } from 'rxjs';
import { CommonConstant } from '../../../../shared/constants/common.constant';
import { STORAGE } from '../../../../shared/storages/storages.inject';

@Injectable({ providedIn: 'root' })
export class GameService {
  constructor(
    @Inject(STORAGE)
    private readonly storage: Storage,
  ) {}

  public collectedElement(input) {
    let userRoomProgress: any = {};

    const userRoomProgressCookie = this.storage.getItem(CommonConstant.USER_ROOM_PROGRESS);
    if (userRoomProgressCookie) {
      userRoomProgress = JSON.parse(userRoomProgressCookie);
    }

    if (!userRoomProgress.roomUserProgresses) {
      userRoomProgress.roomUserProgresses = [];
    }

    userRoomProgress.roomUserProgresses.push({
      gameScene: input.gameScene,
      elementCollected: input.elementCollected,
      time: input.time,
    });

    this.storage.setItem(CommonConstant.USER_ROOM_PROGRESS, JSON.stringify(userRoomProgress));
    return of(true);
  }

  public getLastRoomUserComplete() {
    // TODO: get current game room user
    return of(true);
  }
}

import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ErrorDialogComponent } from '../../../../shared/components/error-dialog/error-dialog.component';
import { BaseGameControlService } from '../../../../shared/services/base-game-control.service';
import { GameTimerCountDownService } from '../../../../shared/services/game-timer-countdown.service';
import { GameService } from './game.service';
import { GameScene } from '../enums/game-sceen.enum';

@Injectable()
export class GameDataService extends BaseGameControlService {
  public readonly SCENE1_DATA = {
    isStartScene1: false,
    isOrangeKeyCollected: false,
    isGreenKeyCollected: false,
    isBlueKeyCollected: false,
  };

  public SCENE2_DATA = {
    isStartScene2: false,
    isRedKeyCollected: false,
    isPurpleKeyCollected: false,
    isYellowKeyCollected: false,
  };

  public SCENE3_DATA = {
    isStartScene3: false,
    isPinkKeyCollected: false,
    isWhiteKeyCollected: false,
    isPuzzle4Solved: false,
    itemMetal: false,
  };

  constructor(
    protected readonly translateService: TranslateService,
    private readonly dialog: MatDialog,
    private readonly gameService: GameService,
    private readonly gameTimerCountDownService: GameTimerCountDownService,
  ) {
    super();
    this.initScene2Puzzle2WrongKeys();
    this.initScene2Puzzle2WrongBooks();
  }

  private initScene2Puzzle2WrongKeys() {
    const NUMBER_OF_KEYS = 5;
    for (let i = 1; i <= NUMBER_OF_KEYS; i++) {
      this.SCENE2_DATA = { ...this.SCENE2_DATA, [`isPuzzle2KeyWrong${i}Collected`]: false };
    }

  }

  private initScene2Puzzle2WrongBooks() {
    const NUMBER_OF_BOOKS = 14;
    for (let i = 1; i <= NUMBER_OF_BOOKS; i++) {
      this.SCENE2_DATA = { ...this.SCENE2_DATA, [`isPuzzle3BookWrong${i}Collected`]: false };
    }
  }

  public async initElement(gameScene: GameScene, elementCollected: string) {
    switch (gameScene) {
      case GameScene.SCENE_1:
        this.SCENE1_DATA[elementCollected] = true;
        break;
      case GameScene.SCENE_2:
        this.SCENE2_DATA[elementCollected] = true;
        break;
      case GameScene.SCENE_3:
        this.SCENE3_DATA[elementCollected] = true;
        break;
    }
  }

  public async collectElement(gameScene: GameScene, elementCollected: string, callback?, skipTimer?: boolean) {
    this.gameTimerCountDownService.stop();

    this.initElement(gameScene, elementCollected);

    // const totalElapsedTime = this.gameTimerCountDownService.getThePastTimer();

    // const lastTime = this.gameTimerCountDownService.getCachePastTime();
    // const timeToCollect = totalElapsedTime - lastTime;
    // this.gameTimerCountDownService.setCachePastTime(totalElapsedTime);

    this.subscribe(this.gameService.collectedElement({
      gameScene,
      elementCollected,
      // time: timeToCollect,
    }), () => {
      if (callback) {
        callback();
      }

      if (!skipTimer) {
        this.gameTimerCountDownService.start();
      }
    });
  }

  public openErrorDialog(keyTranslate: string, onReturn: any): void {
    const config: MatDialogConfig = {
      panelClass: 'error--dialog',
      disableClose: true,
      data: {
        textError: this.translateService.instant(keyTranslate),
        onReturn: () => onReturn()
      }
    };
    this.dialog.open(ErrorDialogComponent, config);
  }
}

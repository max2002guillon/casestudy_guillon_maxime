import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { DragScrollModule } from 'ngx-drag-scroll';
import { CodeInputModule } from '../../../shared/components/code-input/code-input.module';
import { ErrorDialogComponent } from '../../../shared/components/error-dialog/error-dialog.component';
import { GameBoardComponent } from '../../../shared/components/game-board/game-board.component';
import { GameChatBotComponent } from '../../../shared/components/game-chatbot/game-chatbot.component';
import { GameDialogComponent } from '../../../shared/components/game-dialog/game-dialog.component';
import { GameInventoryItemContainerComponent } from '../../../shared/components/game-inventory-item-container/game-inventory-item-container.component';
import { GameInventoryComponent } from '../../../shared/components/game-inventory/game-inventory.component';
import { GameKeyComponent } from '../../../shared/components/game-key/game-key.component';
import { GameNavigationBackComponent } from '../../../shared/components/game-navigation-back/game-navigation-back.component';
import { GameNavigationBackService } from '../../../shared/components/game-navigation-back/services/game-navigation-back.service';
import { GameNavigationComponent } from '../../../shared/components/game-navigation/game-navigation.component';
import { GameSoundComponent } from '../../../shared/components/game-sound/game-sound.component';
import { GameTimerComponent } from '../../../shared/components/game-timer/game-timer.component';
import { SafePipe } from '../../../shared/pipes/safe.pipe';
import { GameBgmService } from '../../../shared/services/audio/game-bgm.service';
import { GameSfxService } from '../../../shared/services/audio/game-sfx.service';
import { GameSoundService } from '../../../shared/services/audio/game-sound.service';
import { GameDialogService } from '../../../shared/services/dialog/game-dialog.service';
import { GameBoardService } from '../../../shared/services/game-board.service';
import { GameChatBotService } from '../../../shared/services/game-chatbot.service';
import { GameHtmlService } from '../../../shared/services/game-html.service';
import { GameHttpService } from '../../../shared/services/game-http.service';
import { GameInventoryService } from '../../../shared/services/game-inventory.service';
import { GameKeyService } from '../../../shared/services/game-key.service';
import { GameNavigationService } from '../../../shared/services/game-navigation.service';
import { GameTimerCountDownService } from '../../../shared/services/game-timer-countdown.service';
import { GameTimerService } from '../../../shared/services/game-timer.service';
import { GameComponent } from './components/game/game.component';
import { GameRoutingModule } from './game-routing.module';
import { GameDataService } from './services/game-data.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    DragDropModule,
    MatDialogModule,
    TranslateModule,
    DragScrollModule,
    GameRoutingModule,
    CodeInputModule,
  ],
  declarations: [
    GameComponent,
    GameBoardComponent,
    GameNavigationComponent,
    GameKeyComponent,
    GameNavigationBackComponent,
    GameTimerComponent,
    GameInventoryComponent,
    GameInventoryItemContainerComponent,
    SafePipe,
    GameDialogComponent,
    ErrorDialogComponent,
    GameSoundComponent,
    GameKeyComponent,
    GameChatBotComponent
  ],
  providers: [
    GameDataService,
    GameHttpService,
    GameHtmlService,
    GameBoardService,
    GameNavigationService,
    GameNavigationBackService,
    GameInventoryService,
    GameDialogService,
    GameKeyService,
    GameTimerService,
    GameSoundService,
    GameBgmService,
    GameSfxService,
    GameKeyService,
    GameChatBotService,
    GameTimerCountDownService
  ],
})
export class GameModule { }

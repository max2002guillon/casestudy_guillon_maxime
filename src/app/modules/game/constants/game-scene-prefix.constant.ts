export class ScenePrefixConstant {
  /* ----------------------------------------- */
  /*                PREFIX SCENE1              */
  /* ----------------------------------------- */
  public static readonly SCENE1_OPENING = 'SCENE1_OPENING';
  public static readonly SCENE1_PUZZLE1 = 'SCENE1_PUZZLE1';
  public static readonly SCENE1_PUZZLE2 = 'SCENE1_PUZZLE2';
  public static readonly SCENE1_PUZZLE3 = 'SCENE1_PUZZLE3';
}

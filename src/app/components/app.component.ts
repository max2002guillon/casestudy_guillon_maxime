import { isPlatformBrowser } from '@angular/common';
import { Component, HostBinding, Inject, PLATFORM_ID } from '@angular/core';
import { environment } from '../../environments/environment';
import { BaseComponent } from '../../shared/components/base-component/base.component';
import { NgSwCheckForUpdateService } from '../../shared/services/service-worker/ngsw-check-for-update.service';
import { NgSwLogUpdateService } from '../../shared/services/service-worker/ngsw-log-update.service';
import { NgSwPromptUpdateService } from '../../shared/services/service-worker/ngsw-prompt-update.service';
import { FavieTranslateService } from '../../shared/translate/servives/favie-translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent extends BaseComponent {
  @HostBinding('style.height') height = '100%';

  constructor(
    private readonly favieTranslateService: FavieTranslateService,
    @Inject(PLATFORM_ID) private readonly platformId,
    private readonly ngSwLogUpdateService: NgSwLogUpdateService,
    private readonly ngSwPromptUpdateService: NgSwPromptUpdateService,
    private readonly ngSwCheckForUpdateService: NgSwCheckForUpdateService,
  ) {
    super();
  }

  onInit() {
    this.favieTranslateService.initAppLang();
    this.initNgSw();
    console.info(`App version: ${environment.version}`);
  }

  private initNgSw() {
    if (environment.production && this.isPlatformBrowser) {
      this.ngSwLogUpdateService.init();
      this.ngSwCheckForUpdateService.init();
      this.ngSwPromptUpdateService.init();
    }
  }

  private get isPlatformBrowser() {
    return isPlatformBrowser(this.platformId);
  }
}

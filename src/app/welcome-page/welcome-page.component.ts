import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';


@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss'],
  animations: [
    trigger('slideInOut', [
    
      state('in', style({
        transform: 'translateX(0)',
      })),
      state('out', style({
        transform: 'translateX(0)',
        
        
      })),
      transition('in => out', animate('4000ms ease-out', keyframes([
        style({ transform: 'translateX(0%)' }),
        style({ transform: 'translateX(-35%)' }),
        style({ transform: 'translateX(0%)' })
      ]))),
      transition('out => in', animate('4000ms ease-in'))
    ])
  ]
})
export class WelcomePageComponent implements OnInit {

  constructor(private router:Router) { }

  backgroundImg: string;
  fogIsland2: string;
  fogIsland3: string;
  cloud: string;
  coolGuys: string;

  opacityValue:number;
  opacityTransition: string;

  canLoadGame1: boolean;

  state:string;

  ngOnInit(): void {
    this.backgroundImg = "../../assets/myPictures/welcome.svg";
    this.fogIsland2 = "../../assets/myPictures/fogIsland02.svg";
    this.fogIsland3 = "../../assets/myPictures/fogIsland03.svg";
    this.cloud = "../../assets/myPictures/cloud.svg";
    this.coolGuys = "../../assets/myPictures/coolGuys.svg";

    this.opacityValue = 1;    
    this.opacityTransition = 'opacity 1s linear';
    this.canLoadGame1 = false;

    this.state = 'in';
    
  }


  onAnimate() {
    this.opacityValue = 0; 
    this.state = this.state === 'in' ? 'out' : 'in';
    this.canLoadGame1 = true;
  }

  loadGame1() {
    if(this.canLoadGame1 == true){
      this.router.navigate(['game1'])     

    }

  }

}

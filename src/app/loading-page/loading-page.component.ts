import { trigger, style, transition, animate} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-loading-page',
  templateUrl: './loading-page.component.html',
  styleUrls: ['./loading-page.component.scss'],
  animations: [
    trigger('imageAnimation', [
      transition(':enter', [
        style({ transform: 'rotateY(0deg)', opacity: 0 }),
        animate('500ms', style({ transform: 'rotate(180deg)', opacity: 1 }))
      ]),
      transition(':leave', [
        animate('500ms', style({ transform: 'rotateY(180deg)', opacity: 0 }))
      ])
    ])
  ]
})
export class LoadingPageComponent implements OnInit {

  constructor(private router:Router) { }

  listImgURL: string[];
  index: number;

  img1:any;
  img2:any;
  img3:any;
  img4:any;
  img5:any;
  img6:any;
  img7:any;

  text:any;

  canClick:boolean;

  ngOnInit(): void {

    this.listImgURL = [
      "../../assets/myPictures/hourglass.svg",
      "../../assets/myPictures/doc.svg",
      "../../assets/myPictures/lamp.svg",
      "../../assets/myPictures/pencil.svg",
      "../../assets/myPictures/balise.svg",
      "../../assets/myPictures/controller.svg",
      "../../assets/myPictures/eeLogo.svg"     
    ];
    this.index = 0;  

    this.img1 = document.getElementById("img1");
    this.img2 = document.getElementById("img2");
    this.img3 = document.getElementById("img3");
    this.img4 = document.getElementById("img4");
    this.img5 = document.getElementById("img5");
    this.img6 = document.getElementById("img6");
    this.img7 = document.getElementById("img7");

    this.text = document.getElementById("text");

    this.img1.classList.add("rotationR");
    this.img2.classList.add("rotationL");
    this.img3.classList.add("rotationR");
    this.img4.classList.add("rotationL");
    this.img5.classList.add("rotationR");
    this.img6.classList.add("rotationL");

    setTimeout(function() {
      this.img1.setAttribute('hidden', true);  
      this.img2.removeAttribute('hidden'); 
      
      setTimeout(function() {
        this.img2.setAttribute('hidden', true);  
        this.img3.removeAttribute('hidden'); 

        setTimeout(function() {
          this.img3.setAttribute('hidden', true);  
          this.img4.removeAttribute('hidden'); 

          setTimeout(function() {
            this.img4.setAttribute('hidden', true);  
            this.img5.removeAttribute('hidden'); 

            setTimeout(function() {
              this.img5.setAttribute('hidden', true);  
              this.img6.removeAttribute('hidden'); 

              setTimeout(() => {
                this.img6.setAttribute('hidden', true);  
                this.img7.removeAttribute('hidden');  
                this.text.removeAttribute('hidden');     
              }, 2000)
  
            }, 2000)

          }, 2000)

        }, 2000)

      }, 2000)

    }, 2000)

    
  }

  loadWelcome():void {    
    this.router.navigate(['welcome']);
    
  }
 
}




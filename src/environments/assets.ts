export const assets = {
  // START TUTORIAL ASSETS

  // END TUTORIAL ASSETS

  PERMANENT_ELEMENT_TIMER: 'assets/images/permanent/timer/timer-bg.png',
  PERMANENT_ELEMENT_NAVIGATION: 'assets/images/permanent/navigation/nav-arrow.svg',
  PERMANENT_ELEMENT_KEY: 'assets/images/permanent/key/key.svg',
  PERMANENT_ELEMENT_NAVIGATION_BACK: 'assets/images/permanent/navigation-back/button-back.svg',
  PERMANENT_ELEMENT_LOADING: 'assets/images/permanent/loading/loading.svg',
  PERMANENT_ELEMENT_INVENTORY_COLLECTOR_OPEN: 'assets/images/permanent/inventory-collector/inventory-collector-open.png',
  PERMANENT_ELEMENT_INVENTORY_COLLECTOR_CLOSED: 'assets/images/permanent/inventory-collector/inventory-collector-closed.png',
  PERMANENT_ICON_MUTE: 'assets/images/permanent/sound/sound-off.png',
  PERMANENT_ICON_UNMUTE: 'assets/images/permanent/sound/sound-on.png',
  PERMANENT_ELEMENT_CLOSE: 'assets/images/permanent/close/btn-close-icon.svg',
  PERMANENT_ELEMENT_CLOSE_CLICKED: 'assets/images/permanent/close/btn-close-clicked-icon.svg',

  BORDER_DIALOG_CODE_WRONG_URL: 'assets/images/permanent/error/border.svg',
  BORDER_DIALOG_CODE_WRONG_MOBILE_URL: 'assets/images/permanent/error/border-mobile.svg',

  SCENE1_OPENING_FRAME_URL: 'assets/images/scene1/opening/scene1-opening-frame.svg',
  SCENE1_OPENING_BACKGROUND_URL: 'assets/public-resources/images/scene1/opening/scene1-opening-background.png',
  SCENE1_OPENING_MOBILE_FRAME_URL: 'assets/images/scene1/opening/scene1-opening-mobile-frame.svg',
  SCENE1_OPENING_MOBILE_BACKGROUND_URL: 'assets/public-resources/images/scene1/opening/scene1-opening-mobile-background.png',

  SCENE1_PUZZLE1_FRAME_URL: 'assets/public-resources/images/scene1/puzzle1/scene1-puzzle1-fg-web2.svg',
  SCENE1_PUZZLE1_BACKGROUND_URL: 'assets/public-resources/images/scene1/puzzle1/scene1-puzzle1-bg2.png',
  SCENE1_PUZZLE1_FRAME_MOBILE_URL: 'assets/public-resources/images/scene1/puzzle1/scene1-puzzle1-fg-mobile2.svg',
  SCENE1_PUZZLE1_BACKGROUND_MOBILE_URL: 'assets/public-resources/images/scene1/puzzle1/scene1-puzzle1-bg-mobile2.png',

  SCENE1_PUZZLE2_FRAME_URL: 'assets/images/scene1/puzzle2/scene1-puzzle2-frame.svg',
  SCENE1_PUZZLE2_MOBILE_FRAME_URL: 'assets/images/scene1/puzzle2/scene1-puzzle2-mobile-frame.svg',
  SCENE1_PUZZLE2_BACKGROUND_URL: 'assets/public-resources/images/scene1/puzzle2/scene1-puzzle2-background.png',
  SCENE1_PUZZLE2_MOBILE_BACKGROUND_URL: 'assets/public-resources/images/scene1/puzzle2/scene1-puzzle2-mobile-bg2.png',

  SCENE1_PUZZLE3_FRAME_DESKTOP_URL: 'assets/images/scene1/puzzle3/scene1-puzzle3-frame.svg',
  SCENE1_PUZZLE3_FRAME_MOBILE_URL: 'assets/images/scene1/puzzle3/scene1-puzzle3-frame-mobile.svg',

  en: {
    PERMANENT_ELEMENT_TIMER: '',

    // START TUTORIAL ASSETS
    TUTORIAL_TITLE_STEP_1: 'TUTORIAL_TITLE_STEP_1',
    TUTORIAL_CONTENT_STEP_1: 'TUTORIAL_CONTENT_STEP_1',
    TUTORIAL_TITLE_STEP_2: 'TUTORIAL_TITLE_STEP_2',
    TUTORIAL_CONTENT_STEP_2: 'TUTORIAL_CONTENT_STEP_2',
    TUTORIAL_TITLE_STEP_3: 'TUTORIAL_TITLE_STEP_3',
    TUTORIAL_CONTENT_STEP_3: 'TUTORIAL_CONTENT_STEP_3',
    TUTORIAL_TITLE_STEP_4: 'TUTORIAL_TITLE_STEP_4',
    TUTORIAL_CONTENT_STEP_4: 'TUTORIAL_CONTENT_STEP_4',

    TUTORIAL_TITLE_STEP_5: 'TUTORIAL_TITLE_STEP_5',
    TUTORIAL_CONTENT_STEP_5: 'TUTORIAL_CONTENT_STEP_5',
    TUTORIAL_TITLE_STEP_6: 'TUTORIAL_TITLE_STEP_6',
    TUTORIAL_CONTENT_STEP_6: 'TUTORIAL_CONTENT_STEP_6',
    TUTORIAL_TITLE_STEP_7: 'TUTORIAL_TITLE_STEP_7',
    TUTORIAL_CONTENT_STEP_7: 'TUTORIAL_CONTENT_STEP_7',
    // END TUTORIAL ASSETS
  },

  fr: {
    PERMANENT_ELEMENT_TIMER: '',

    // START TUTORIAL ASSETS

    // END TUTORIAL ASSETS
  }
};
